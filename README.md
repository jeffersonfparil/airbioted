# airbioted

**AIRBIOTED**: analysing aerobiological samples based on sequences of taxonomy-discriminating amplicons

## Inputs

|Parameters|Description|
|:---------:|:-----------------|
|INPUT_DIR | directory where all the fastq files are located with filenames formatted as: **\{SOME_IDENTIFIER_STRING\}--T#_R#.fastq.gz**; where **T#** is the time point e.g. T1 and T34, while **R#** is the read number i.e. R1 or R2|
|FORWARD_PRIMERS_LIST<sup>[*](#footnote)</sup> | plain text file where all the forward primer/s is/are listed one line at a time (must have the same number of lines as the reverse primer/s file)|
|REVERSE_PRIMERS_LIST<sup>[*](#footnote)</sup> | plain text file where all the reverse primer/s is/are listed one line at a time (must have the same number of lines as the forward primer/s file)|
|MIN_LENGTH | minimum sequence (amplicon) length|
|MAX_LENGTH | maximum sequence (amplicon) length|
|BLASTDB_LIST | plain text file listing the full path including the filename of the BLAST database/s to use|
|GI_EXCLUSION_LIST | list of GI to be excluded from the BLASTn output because they are too ambiguous, e.g. environmental samples|
|PERC_IDENTITY | minimum sequence identity in percentage to be included in the BLASTn output|
|FILTER_KEYWORDS_LIST | plain text file list of keywords to filter NCBI organism (subject) names to improve filtering (one keyword per line)|
|TAXON_DUMP_DIR | directory containing the taxonomy dump files: **nodes.dmp** and **names.dmp** for obtaining the taxonomic information given species ID number|
|PHYLUM | phylum of interest|
|DIVISION_FAMILY_LIST | plain text file listing each family and the corresponding division with the headers: "FAMILY" and "DIVISION", respectively|
|OUTPUT_DIR | existing directory where the output will be written into|

<a name="footnote">*</a>: Will not treat the amplicons of each primer pair separately. The resulting trimmed amplicons from all the primer pairs in the forward and reverse primers list will be pooled per time point. This is to accomodate pooling of amplicon sequences from two or more primer pairs. For amplicons that need to be considered seprately, please create separate forward and reverse primers lists and run the pipeline separately for each amplicon dataset.

## Outputs
### Raw output

- trimmed fastq.gz files
- merged fastq files
- filtered fastq and fasta files
- derepliplicated fasta files
- clustering OTU fasta files
- taxonomy blastn hits files
- sequence statistics per file

### Summarised output

- Heatmaps of abundance data:
    + Airbiota_PER_PHYLUM.jpg
    + Airbiota_PER_SUBPHYLUM.jpg
    + Airbiota_PER_CLASS.jpg
    + Airbiota_PER_FAMILY_set*.jpg
    + Airbiota_PER_GENUS_set*.jpg
    + Airbiota_PER_SPECIES_set*.jpg
    + \{PHYLUM_OF_INTEREST\}_\{DIVISIONS\}_FAMILIES.jpg
- Tables of abundances, counts, correlations and OTU sequences:
    + Consolidated_taxon_counts_across_time.csv
    + \{PHYLUM_OF_INTEREST\}_for_ASV_table.csv
    + \{PHYLUM_OF_INTEREST\}_CORR_Rho.csv
    + \{PHYLUM_OF_INTEREST\}_CORR_p-val.csv
    + \{PHYLUM_OF_INTEREST\}_TAXON_ABUNDANCE_PER_WEEK.csv
    + \{PHYLUM_OF_INTEREST\}_OTU_COUNTS_PER_TAXON_PER_WEEK.csv
    + \{PHYLUM_OF_INTEREST\}_OTU_ID_PER_TAXON_PER_WEEK.csv
    + \{PHYLUM_OF_INTEREST\}_OTU_SEQUENCES.fasta
- Statistics:
    + Histograms_number_of_sequences.jpg
    + Histograms_sequences_length.jpg
    + Histograms_error_rate.jpg
    + Consolidated_blastn_output_stats.jpeg
    + Stats_blastn_consensus_identification.jpg
    + Stats_blastn_consensus_identification.csv
    + STATS_RAW_READS.csv
    + STATS_TRIMMED_READS.csv
    + STATS_MERGED_READS.csv
    + STATS_FILTERED_READS.csv

## Workflow

<img src="/misc/workflow.jpg">