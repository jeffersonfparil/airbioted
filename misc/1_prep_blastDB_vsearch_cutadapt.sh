#!/bin/bash

BLASTDB=$1

### download all of NCBI's nucleotide database
mkdir $BLASTDB
cd $BLASTDB
wget ftp://ftp.ncbi.nlm.nih.gov/blast/db/nt*.tar.gz
for i in $(ls *.tar.gz)
do
tar -xvzf $i
done

### download the Viridiplantae ITS2 database from http://its2.bioapps.biozentrum.uni-wuerzburg.de/
### and builb ITS2 Viridiplantae BLAST database
makeblastdb -in Virdieplanta_20180910.fasta -dbtype 'nucl' -out VIRIDIPLANTAE -parse_seqids

### prepare and merge the NCBI [and ITS2] databases --> merging possible but environmental and uncultured samples exclusion during blast is not possible since the ITS2 database is not correctly (NCBI-standard) formatted so we'll be doing a step-wise blast again
# blastdb_aliastool -dblist "VIRIDIPLANTAE nt.00 nt.01 nt.02 nt.03 nt.04 nt.05 nt.06 nt.07 nt.08 nt.09 nt.10 nt.11 nt.12 nt.13 nt.14 nt.15 nt.16 nt.17 nt.18 nt.19 nt.20 nt.21 nt.22 nt.23 nt.24 nt.25 nt.26 nt.27 nt.28 nt.29 nt.30 nt.31 nt.32 nt.33 nt.34 nt.35 nt.36 nt.37 nt.38 nt.39 nt.40 nt.41 nt.42 nt.43 nt.44 nt.45 nt.46 nt.47 nt.48 nt.49 nt.50 nt.51 nt.52 nt.53 nt.54 nt.55 nt.56 nt.57 nt.58 nt.59 nt.60" \
blastdb_aliastool -dblist "nt.00 nt.01 nt.02 nt.03 nt.04 nt.05 nt.06 nt.07 nt.08 nt.09 nt.10 nt.11 nt.12 nt.13 nt.14 nt.15 nt.16 nt.17 nt.18 nt.19 nt.20 nt.21 nt.22 nt.23 nt.24 nt.25 nt.26 nt.27 nt.28 nt.29 nt.30 nt.31 nt.32 nt.33 nt.34 nt.35 nt.36 nt.37 nt.38 nt.39 nt.40 nt.41 nt.42 nt.43 nt.44 nt.45 nt.46 nt.47 nt.48 nt.49 nt.50 nt.51 nt.52 nt.53 nt.54 nt.55 nt.56 nt.57 nt.58 nt.59 nt.60" \
-dbtype nucl \
-out NCBI_NT0060 \
-title "MERGED NCBI DATABASES"

### download the ~manually extracted exclusion list for the NCBI database from "misc/EXCLUDE_environ_uncult.gi"

# ### download catalogue of life - taxonomy database (alt: https://www.itis.gov/downloads/itisMySQLTables.tar.gz)
# wget http://www.catalogueoflife.org/DCA_Export/zip-fixed/2018-11-29-archive-complete.zip
# unzip 2018-11-29-archive-complete.zip #taxa list is "taxa.txt"
# #convert into RDS (R binary data file) for faster loading times
# #first remove unnecessary columns
# mv taxa.txt taxa.txt.bk
# cut -d$'\t' -f1,10,11,12,13,14,16,18 taxa.txt.bk > taxa.txt

# echo -e "args=commandArgs(trailing=TRUE)" > taxa_RDS_convert.r
# echo -e "f = args[1]" >> taxa_RDS_convert.r
# echo -e "taxa = read.delim(f, header=TRUE, sep='\t')" >> taxa_RDS_convert.r
# echo -e "saveRDS(taxa, file=paste0(strsplit(f, '[.]')[[1]][1], '.rds'))" >> taxa_RDS_convert.r
# Rscript taxa_RDS_convert.r taxa.txt
# rm taxa_RDS_convert.r

## alternative: ncbi dump files of taxonomy database
wget https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz
tar -xvzf new_taxdump.tar.gz
# wget ftp.ncbi.nlm.nih.gov/pub/taxonomy/gi_taxid_nucl.dmp.gz
# gunzip -d gi_taxid_nucl.dmp.gz

echo -e "install.packages('taxonomizr')" > install_taxonomizr.r
echo -e "install.packages('VennDiagram')" >> install_taxonomizr.r
echo -e "install.packages('BiocManager')" >> install_taxonomizr.r
echo -e "install.packages(BiocManager::install('Biostrings'))" >> install_taxonomizr.r
Rscript install_taxonomizr.r
rm install_taxonomizr.r
# library("taxonomizr")
# nodes=read.nodes("/data/BlastDB/nodes.dmp")
# names=read.names("/data/BlastDB/names.dmp")
# test_Scheffelia_dubia = getTaxonomy2(3190, nodes, names, mc.cores=8)
# test_Hyphodontia_radula = getTaxonomy2(98761, nodes, names, mc.cores=8)

# #alternative to taxonomizr
# wget https://github.com/shenwei356/taxonkit/releases/download/v0.3.0/taxonkit_linux_amd64.tar.gz
# tar -xvzf taxonkit_linux_amd64.tar.gz
# ./taxonkit lineage --nodes nodes.dmp --names names.dmp 1915895
# mkdir /home/student.unimelb.edu.au/jparil/.taxonkit
# cp nodes.dmp /home/student.unimelb.edu.au/jparil/.taxonkit
# cp names.dmp /home/student.unimelb.edu.au/jparil/.taxonkit

### install blast, vsearch and cutadapt
sudo apt install ncbi-blast+
sudo apt install vsearch
sudo apt install cutadapt


# ###misc for downloading ncbi taxonomy database
# sudo apt install libxml2-dev
# sudo apt install libssl-dev
# install.packages("BiocManager")
# BiocManager::install("RSQLite")
# BiocManager::install("biomaRt")
# BiocManager::install("edgeR")
# install.packages("myTAI")
# biomartr::download.database.all(db = "taxdb", path = "taxdb") #which actually already part of the nt databases!!!