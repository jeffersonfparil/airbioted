#!/bin/bash

INPUT_DIR=$1
OUTPUT_DIR=$2
BLASTDB=$3

###############################################################
###															###
### PRE-PROCESSING, CLUSTERING, AND TAXONOMY IDENTIFICATION ###
###															###
###############################################################
### The main script within with parallelization of the sub-script "2a_preproc_cluster_taxon__.sh"
### and its components are run.

### test
# cd /data/Misc/AIRBIOTED/illumina/src/airbioted/src
# INPUT_DIR=/data/Misc/AIRBIOTED/illumina/fastq
# OUTPUT_DIR=/data/Misc/AIRBIOTED/illumina
# BLASTDB=/data/BlastDB

###############################
### setup working directory ###
###############################
mkdir ${OUTPUT_DIR}/trimmed
mkdir ${OUTPUT_DIR}/merged
mkdir ${OUTPUT_DIR}/filtered
mkdir ${OUTPUT_DIR}/derepli
mkdir ${OUTPUT_DIR}/cluster
mkdir ${OUTPUT_DIR}/taxonomy

##############################################
###									 	   ###
### PRE-PROCESS, CLUSTER AND COUNT SPECIES ###  
###										   ###
##############################################
### parallelize for each sample (read-pair) the:
##		(1.) trimming off of primer sequences with cutadapt
##		(2.) merging of the paired-end reads
##		(3.) filtering out of reads with greater than 2 expected errors (i.e. sum(P[error]), where P[E] ~ 10^(-PHRED scrores/10))
##		(4.) dereplicate sequences (i.e. non-uniqu sequences are moved and the counts number are noted)
##		(5.) assessment of resulting pre-processed sequences
##		(6.) clustering into amplicon sequence variants (ASVs) to generate a list of consensus sequences per cluster (group sequences 90% similar)
##		(7.) count the number of occurences of each consensus sequence in the pre-processed sequences
##		(8.) blast the consensus sequences to determine the species of origin (show all hits >= 80 similarity and no shortlisting)
##		(9.) consolidate blast output such that a single subject with the highest bit score (accounts for both the coverage and e-value) with similarity with similarity at least 80% is selected
##		(main_outputs) 
#			- list of species corresponding to the consensus sequences observed
#			- count data of each consensus sequence
parallel --link ./2a_preproc_cluster_taxon.sh {1} {2} $BLASTDB $OUTPUT_DIR ::: $(ls ${INPUT_DIR}/*R1*.fastq.gz) ::: $(ls ${INPUT_DIR}/*R2*fastq.gz)
#	INPUT:
#		(1) FILENAME_R1 = ${INPUT_DIR}/sequence_read_1.fastq.tar.gz
#		(2) FILENAME_R2 = ${INPUT_DIR}/sequence_read_2.fastq.tar.gz
#		(3) BLASTDB = ${BLASTDB}
#		(4) OUTPUT_DIR = ${OUTPUT_DIR}
#	OUTPUT:
#		- trimmed, merged, filtered, dereplicated sequence files
#		- consensus sequences defining the clusters and corresponding species
#		- count data of consensus sequences in the filtered amplicons
#		- species count data
#		- sequence statistics: ${INPUT_DIR}/fastq/*.stats.csv & ${OUTPUT_DIR}/*/*.stats.csv

#################################
###							  ###
### PRE-PROCESSING STATISTICS ###
###							  ###
#################################
# consolidate stats of raw, trimmed, merged and filtered amplicons
cat ${INPUT_DIR}/*.stats.csv > ${OUTPUT_DIR}/STATS_RAW_READS.csv
cat ${OUTPUT_DIR}/trimmed/*.stats.csv > ${OUTPUT_DIR}/STATS_TRIMMED_READS.csv
cat ${OUTPUT_DIR}/merged/*.stats.csv > ${OUTPUT_DIR}/STATS_MERGED_READS.csv
cat ${OUTPUT_DIR}/filtered/*.stats.csv > ${OUTPUT_DIR}/STATS_FILTERED_READS.csv
./2b_consolidate_stats.r ${OUTPUT_DIR}

###############################
###							###
### CONSOLIDATE TAXA COUNTS ###
###							###
###############################
### consolidate the species list and count data across all samples (read-pairs)
for AMPLICON in ITS trnLF
do
./2c_consolidate_taxon_counts_across_time.r 	${OUTPUT_DIR}/cluster \
												${OUTPUT_DIR}/taxonomy \
												$BLASTDB \
												${OUTPUT_DIR}/taxonomy \
												$AMPLICON
done
#	INPUT:
# 		(1) COUNTS_DIR = ${OUTPUT_DIR}/cluster
# 		(2) TAXON_DIR = ${OUTPUT_DIR}/taxonomy
#		(3) BLASTDB = ${BLASTDB} --> containing the NCBI taxonomy dump files i.e. nodes.dmp & names.dmp
# 		(4) OUTPUT_DIR = ${OUTPUT_DIR}/taxonomy
# 		(5) AMPLICON = ITS or trnLF

# sample execution:
# ./2_preproc_cluster_taxon_consolidate.sh  /data/Misc/AIRBIOTED/illumina/fastq/ /data/Misc/AIRBIOTED/illumina/ /data/BlastDB/
