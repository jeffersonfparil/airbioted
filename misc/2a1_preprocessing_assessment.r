#!/usr/bin/env Rscript

#######################################
### ASSESS FASTQ SEQUENCE QUALITIES ###
#######################################
### summarize qualities and draw figures

args = commandArgs(trailing = TRUE)
STATS_FILE=args[1]

# #test
# STATS_FILE="/data/Misc/Airbiota_Ed/vsearch_out//filtered/W9A_S9.stats"
# STATS_FILE="/volume1/AIRBIOTED/fastq/W10A_S10_R1.stats"

DIR = strsplit(STATS_FILE, split="/")
DIR = paste(DIR[[1]][1:length(DIR[[1]])-1], collapse="/")
setwd(DIR)

system(paste0("csplit --suppress-matched ", STATS_FILE, " '/^$/' {*} -f ", paste0(strsplit(basename(STATS_FILE), split="[.]")[[1]][1], "_")))

system(paste0("sed -i 's/>=//g' ", strsplit(basename(STATS_FILE), split="[.]")[[1]][1], "_01"))
LENGTHS = read.table(paste0(strsplit(basename(STATS_FILE), split="[.]")[[1]][1], "_01"), header=FALSE, skip=3)
colnames(LENGTHS) = c("L", "N", "Pct", "AccPct")
LENGTHS$Pct = LENGTHS$N*100/sum(LENGTHS$N)
# plot(x=LENGTHS$L, y=LENGTHS$Pct, xlab="Amplicon Length (bp)", ylab="Frequency (%)", type="b", pch=20)

system(paste0("sed -i \"s/'/xxx/g\" ", strsplit(basename(STATS_FILE), split="[.]")[[1]][1], "_02"))
QUALITIES = read.table(paste0(strsplit(basename(STATS_FILE), split="[.]")[[1]][1], "_02"), sep="", header=FALSE, skip=3)
colnames(QUALITIES) = c("ASCII", "Q", "Pe", "N", "Pct", "AccPct")
QUALITIES$Pct = QUALITIES$N*100/sum(QUALITIES$N)
# plot(x=QUALITIES$Pe, y=QUALITIES$Pct, xlab="p-value", ylab="Frequency (%)", type="b", pch=20)

NSEQ = sum(LENGTHS$N)
ALEN = sum(LENGTHS$L * LENGTHS$Pct/100)
AQUA = sum(QUALITIES$Pe * QUALITIES$Pct/100)

jpeg(paste0(STATS_FILE, ".jpg"), quality=100, width=1500, height=700)
par(mfrow=c(1,2), cex=2)
plot(x=LENGTHS$L, y=LENGTHS$Pct, xlab="Amplicon Length (bp)", ylab="Frequency (%)", main="Amplicon Length Distribution", type="b", pch=20, col="red")
legend("topleft", legend=c(paste0("Total sequences = ", NSEQ), paste0("Mean length (bp) = ", round(ALEN, 0))))
plot(x=QUALITIES$Pe*100, y=QUALITIES$Pct, xlab="Error Rate (%)", ylab="Frequency (%)", main="Read Quality Distribution", type="b", pch=20, col="blue")
legend("topright", legend=paste0("Mean error rate = ", round(AQUA*100, 2), "%"))
dev.off()

write.table(data.frame(strsplit(basename(STATS_FILE), ".stats")[[1]][1], NSEQ, ALEN, AQUA), file=paste0(STATS_FILE, ".csv"), sep=",", col.names=FALSE, row.names=FALSE)

#cleaup
system(paste0("rm ", strsplit(basename(STATS_FILE), split="[.]")[[1]][1], "_*"))
