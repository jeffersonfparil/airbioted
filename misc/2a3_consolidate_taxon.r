#!/usr/bin/env Rscript

###########################
### MERGE BLAST OUTPUTS ###
###########################
### Merging blast output from the IT2 Viridiplantae and the 61 NCBI nucleotide databases
### into a single blast output such that we only have 1 top hit per concensus sequence.
### Taxon details will be added in the next next script: "2c_consolidate_taxon_counts_across_time.r"

args = commandArgs(trailing = TRUE)
BLAST_DIR=args[1]
NAME=args[2]
# #test
# BLAST_DIR="/data/Misc/AIRBIOTED/illumina/taxonomy/"
# NAME="W34A_S34_trnLF"
# NAME="W10A_S10_ITS"

setwd(BLAST_DIR)
BLAST_LIST = system(paste0("ls ", NAME,".*.blastemp"), intern=TRUE)
# ###################################################################
# ### for ITS2 amplicons use only the ITS2 Viridiplantae database ###
# ###################################################################
# if (tail(strsplit(NAME, "_")[[1]], 1)=="ITS") {
# 	BLAST_LIST = BLAST_LIST[grepl("VIRIDIPLANTAE", BLAST_LIST)]
# }
# ################################################################### test this again and lower the query coverage for ITS amplicons

### merge blastoutput from the 2 databases
rm(list=c("MERGED"))
for (i in 1:length(BLAST_LIST)){
	fname = BLAST_LIST[i]
	if (file.info(fname)$size==0){
		next
	} else {
		b = read.delim(fname, header=FALSE, sep="\t")
		colnames(b) = c("QUERY_ID", "SUBJECT_ID", "PERCENT_IDENTITY", "E_VALUE", "QUERY_COVERAGE", "BITSCORE", "ORGANISM")
		### filtering based on amplicon type and expected blast output keywords from the NCBI databases
		if (grepl("NCBI_NT0060", fname)==TRUE & tail(strsplit(NAME, "_")[[1]], 1)=="ITS") {
			### filtering out non-ITS amplicons when dealing with ITS sequences
			filtering_index = grepl("ITS", b$ORGANISM) | grepl("ITS2", b$ORGANISM) | grepl("ITS1", b$ORGANISM) | grepl("internal transcribed spacer", b$ORGANISM) | grepl("Internal transcribed spacer", b$ORGANISM) | grepl("Internal Transcribed spacer", b$ORGANISM) | grepl("Internal Transcribed Spacer", b$ORGANISM) | grepl("complete sequence", b$ORGANISM) | grepl("Complete sequence", b$ORGANISM) | grepl("Complete Sequence", b$ORGANISM) | grepl("complete genome", b$ORGANISM) | grepl("Complete genome", b$ORGANISM) | grepl("Complete Genome", b$ORGANISM)
			b = b[filtering_index,]
		} else if (grepl("NCBI_NT0060", fname)==TRUE & tail(strsplit(NAME, "_")[[1]], 1)=="trnLF"){
			### filtering out non-trnL-trnF amplicons when dealing with trnL-trnF amplicon sequences
			filtering_index = grepl("trnL-trnF", b$ORGANISM) | grepl("trnL", b$ORGANISM) | grepl("trnF", b$ORGANISM) | grepl("Chloroplast", b$ORGANISM) | grepl("chloroplast", b$ORGANISM) | grepl("complete sequence", b$ORGANISM) | grepl("Complete sequence", b$ORGANISM) | grepl("Complete Sequence", b$ORGANISM) | grepl("complete genome", b$ORGANISM) | grepl("Complete genome", b$ORGANISM) | grepl("Complete Genome", b$ORGANISM)
			b = b[filtering_index,]
		}
		extract_consensus_name <- function(x) {strsplit(as.character(x), ";")[[1]][1]}
		id = sapply(b$QUERY_ID, FUN=extract_consensus_name)
		extract_species_name <- function(x) {paste(strsplit(as.character(x), " ")[[1]][1:2], collapse=" ")}
		species = sapply(b$ORGANISM, FUN=extract_species_name)
		extract_taxid <- function(x){
			if(grepl(";", x)){
				out = as.numeric(strsplit(as.character(x), ";")[[1]][1])
			} else {
				out = as.numeric(as.character(x))
			}
			return(out)
		}
		taxid = sapply(b$SUBJECT_ID, FUN=extract_taxid)
		b = data.frame(DATABASE=rep(strsplit(basename(BLAST_LIST[i]), split="[.]")[[1]][2], times=nrow(b)),
						QUERY_ID=id, 
						PERCENT_IDENTITY=b$PERCENT_IDENTITY, 
						E_VALUE=b$E_VALUE, 
						QUERY_COVERAGE=b$QUERY_COVERAGE, 
						BITSCORE=b$BITSCORE, 
						ORGANISM=species,
						ORGANISM_ID=taxid)
		b$ORGANISM_ID
		if(exists("MERGED")==FALSE){
			MERGED = b
		} else {
			MERGED = rbind(MERGED, b)
		}
	}
}

### identify the amplicons
NAME_SPLIT = strsplit(NAME, "_")[[1]]
AMPLICON_NAME = NAME_SPLIT[length(NAME_SPLIT)]

### select the subject with the highest bit score instead of just identity
QUERY_ID = levels(MERGED$QUERY_ID)
PERCENT_IDENTITY = c()
QUERY_COVERAGE = c()
BITSCORE = c()
ORGANISM = c()
ORGANISM_ID = c()
DATABASE = c()
for (id in QUERY_ID){
	# print(id)
	sub = subset(MERGED, QUERY_ID==id)
	sub = sub[!grepl("Uncultured", sub$ORGANISM) & !grepl("uncultured", sub$ORGANISM) & !grepl("Fungal", sub$ORGANISM) & !grepl("environmental", sub$ORGANISM) & !grepl("Environmental", sub$ORGANISM), ] #just in case the exclusion GI list in blastn does not work :-/
	# if (AMPLICON_NAME == "trnLF"){
	# 	SUB = sub[sub$DATABASE=="VIRIDIPLANTAE",]
	# } else {
	# 	SUB = sub #because ITS2 amplicons can be present in both plants and fungi
	# }
	# if(nrow(SUB)==0){
	# 	# sub = sub[sub$PERCENT_IDENTITY==max(sub$PERCENT_IDENTITY, na.rm=TRUE),]
	# 	sub = sub[sub$BITSCORE==max(sub$BITSCORE, na.rm=TRUE),]
	# 	SUB = sub[sub$E_VALUE==min(sub$E_VALUE, na.rm=TRUE),]
	# }
	# SUB = SUB[order(sub$BITSCORE, decreasing=TRUE), ]
	### remove hits with less than 75% query coverage for trnL-trnF and just 60% for ITS amplicons
	# if (AMPLICON_NAME=="ITS") {
	# 	sub = sub[sub$QUERY_COVERAGE >= 60, ]
	# } else {
	# 	sub = sub[sub$QUERY_COVERAGE >= 75, ]
	# } # NOPE! WE'RE GETTING VIRIDIPLANTAE DB hits with too low %coverage!
	sub = sub[sub$QUERY_COVERAGE >= 75, ]
	sub = sub[order(sub$BITSCORE, decreasing=TRUE), ]
	SUB = sub[1,]
	PERCENT_IDENTITY = c(PERCENT_IDENTITY, SUB$PERCENT_IDENTITY)
	QUERY_COVERAGE = c(QUERY_COVERAGE, SUB$QUERY_COVERAGE)
	BITSCORE = c(BITSCORE, SUB$BITSCORE)
	ORGANISM = c(ORGANISM, as.character(SUB$ORGANISM))
	ORGANISM_ID = c(ORGANISM_ID, SUB$ORGANISM_ID)
	DATABASE = c(DATABASE, as.character(SUB$DATABASE))
}

BLAST_MERGED = data.frame(QUERY_ID, PERCENT_IDENTITY, QUERY_COVERAGE, BITSCORE, ORGANISM, ORGANISM_ID, DATABASE)
BLAST_MERGED = BLAST_MERGED[order(BLAST_MERGED$ORGANISM),]
# # remove hits with less than 75% query coverage
# BLAST_MERGED = BLAST_MERGED[BLAST_MERGED$QUERY_COVERAGE >= 75, ]

# system(paste0("rm ", NAME,".*.blastout"))
write.table(BLAST_MERGED, file=paste0(NAME, ".blastout"), sep="\t", row.names=FALSE)
