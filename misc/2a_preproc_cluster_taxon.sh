#!/bin/bash

#######################################################
### vsearch clustering and taxonomic classification ###
####################################################### paralellizable sub-script of the main script: "2_preproc_cluster_taxon_consolidate.sh"
### parallelizable bash script to:
###		- assess sequence qualities
###		- trim-off primers and group by amplicon type
###		- merging the 2 reads (paired-end reads)
###		- filtering by quality and length
###		- dereplication
###		- clustering by consensus sequences and counting the number of occurence of each concensus sequence
###		- BLAST each consensus sequence to identify taxon
### per fastq read pair or per week.

#############
### INPUT ###
#############
FILENAME_R1=$1
FILENAME_R2=$2
BLASTDB=$3
OUTPUT_DIR=$4

#test:
# FILENAME_R1=/data/Misc/AIRBIOTED/illumina/fastq/W1A_S1_L001_R1_001.fastq.gz
# FILENAME_R2=/data/Misc/AIRBIOTED/illumina/fastq/W1A_S1_L001_R2_001.fastq.gz
# BLASTDB=/data/BlastDB
# OUTPUT_DIR=/data/Misc/AIRBIOTED/illumina/

BASE_R1=$(basename $FILENAME_R1)
BASE_R2=$(basename $FILENAME_R2)
INPUT_DIR=${FILENAME_R1%${BASE_R1}*}
NAME=${BASE_R1%_L001_R1_001.fastq.gz*}

###############################
### sequence pre-processing ###
###############################
### 1.) assess sequence qualities
vsearch --fastq_stats $FILENAME_R1 \
		--log $(dirname $FILENAME_R1)/${NAME}_R1.stats
vsearch --fastq_stats $FILENAME_R2 \
		--log $(dirname $FILENAME_R2)/${NAME}_R2.stats
#try plotting some pretty graphs here....
./2a1_preprocessing_assessment.r $(dirname $FILENAME_R1)/${NAME}_R1.stats
./2a1_preprocessing_assessment.r $(dirname $FILENAME_R2)/${NAME}_R2.stats

### 2.) trim-off primers (IT2 & trnL-trnF sequences) and group by ITS1$2 and trnL-trnF amplicons
# #2.a.) list primers
#forwards (5'-3'):
ITS5="GGAAGTAAAAGTCGTAACAAGG"
ITS5_C=$(./2a2_complement_or_reverseComplement.r C $ITS5 | cut -d" " -f2 | sed 's/"//g')
S6="ATATCTCGGCTCTTGCATCG"
S6_C=$(./2a2_complement_or_reverseComplement.r C $S6 | cut -d" " -f2 | sed 's/"//g')
TabE="GGTTCAAGTCCCTCTATCCC"
TabE_C=$(./2a2_complement_or_reverseComplement.r C $TabE | cut -d" " -f2 | sed 's/"//g')
## reverses (5'-3'):
ITS4="TCCTCCGCTTATTGATATGC"
ITS4_C=$(./2a2_complement_or_reverseComplement.r C $ITS4 | cut -d" " -f2 | sed 's/"//g')
TabF="ATTTGAACTGGTGACACGAG"
TabF_C=$(./2a2_complement_or_reverseComplement.r C $TabF | cut -d" " -f2 | sed 's/"//g')
#2.c.) trim and group
######################## ITS1 & ITS2 ########################
cutadapt --discard-untrimmed \
		-g $ITS5 \
		-a $ITS4_C \
		-G $ITS4 \
		-A $ITS5_C \
		-n 2 \
		-o ${OUTPUT_DIR}/trimmed/${BASE_R1%_*}_ITS12.fastq.gz \
		-p ${OUTPUT_DIR}/trimmed/${BASE_R2%_*}_ITS12.fastq.gz \
		${INPUT_DIR}/${BASE_R1} \
		${INPUT_DIR}/${BASE_R2}
######################## ITS2 alone ########################
cutadapt --discard-untrimmed \
		-g $S6 \
		-a $ITS4_C \
		-G $ITS4 \
		-A $S6_C \
		-n 2 \
		-o ${OUTPUT_DIR}/trimmed/${BASE_R1%_*}_ITS2.fastq.gz \
		-p ${OUTPUT_DIR}/trimmed/${BASE_R2%_*}_ITS2.fastq.gz \
		${INPUT_DIR}/${BASE_R1} \
		${INPUT_DIR}/${BASE_R2}
# merge ITS1&2 and ITS2 alone
cat ${OUTPUT_DIR}/trimmed/${BASE_R1%_*}_ITS12.fastq.gz ${OUTPUT_DIR}/trimmed/${BASE_R1%_*}_ITS2.fastq.gz > ${OUTPUT_DIR}/trimmed/${BASE_R1%_*}_ITS.fastq.gz
cat ${OUTPUT_DIR}/trimmed/${BASE_R2%_*}_ITS12.fastq.gz ${OUTPUT_DIR}/trimmed/${BASE_R2%_*}_ITS2.fastq.gz > ${OUTPUT_DIR}/trimmed/${BASE_R2%_*}_ITS.fastq.gz
rm ${OUTPUT_DIR}/trimmed/${BASE_R1%_*}_ITS12.fastq.gz ${OUTPUT_DIR}/trimmed/${BASE_R1%_*}_ITS2.fastq.gz ${OUTPUT_DIR}/trimmed/${BASE_R2%_*}_ITS12.fastq.gz ${OUTPUT_DIR}/trimmed/${BASE_R2%_*}_ITS2.fastq.gz
######################## trnL-trnF ########################
cutadapt --discard-untrimmed \
		-g $TabE \
		-a $TabF_C \
		-G $TabF \
		-A $TabE_C \
		-n 2 \
		-o ${OUTPUT_DIR}/trimmed/${BASE_R1%_*}_trnLF.fastq.gz \
		-p ${OUTPUT_DIR}/trimmed/${BASE_R2%_*}_trnLF.fastq.gz \
		${INPUT_DIR}/${BASE_R1} \
		${INPUT_DIR}/${BASE_R2}
#2.d.) assess qualities
for i in ITS trnLF
do
vsearch --fastq_stats ${OUTPUT_DIR}/trimmed/${BASE_R1%_*}_${i}.fastq.gz \
		--log ${OUTPUT_DIR}/trimmed/${BASE_R1%_*}_${i}.stats
vsearch --fastq_stats ${OUTPUT_DIR}/trimmed/${BASE_R2%_*}_${i}.fastq.gz \
		--log ${OUTPUT_DIR}/trimmed/${BASE_R2%_*}_${i}.stats
./2a1_preprocessing_assessment.r ${OUTPUT_DIR}/trimmed/${BASE_R1%_*}_${i}.stats
./2a1_preprocessing_assessment.r ${OUTPUT_DIR}/trimmed/${BASE_R2%_*}_${i}.stats
done
#peek:
cat ${OUTPUT_DIR}/trimmed/*.csv

### 3.) merge paired-reads (and assess qualities)
for i in ITS trnLF
do
vsearch --fastq_mergepairs ${OUTPUT_DIR}/trimmed/${BASE_R1%_*}_${i}.fastq.gz \
		--reverse ${OUTPUT_DIR}/trimmed/${BASE_R2%_*}_${i}.fastq.gz \
		--fastqout ${OUTPUT_DIR}/merged/${NAME}_${i}.fastq
vsearch --fastq_stats ${OUTPUT_DIR}/merged/${NAME}_${i}.fastq \
		--log ${OUTPUT_DIR}/merged/${NAME}_${i}.stats
./2a1_preprocessing_assessment.r ${OUTPUT_DIR}/merged/${NAME}_${i}.stats
done
#peek:
cat ${OUTPUT_DIR}/merged/*.csv

### 4.) quality and sequence length filtering 
### Note: query lengths < 200bp seem to result in low quality hits,
###		  e.g. Pinnaceae ITS amplicon (168bp) getting identified as trnL-trnF partial sequence with coverage ~50% only!
for i in ITS trnLF
do
vsearch --fastq_filter ${OUTPUT_DIR}/merged/${NAME}_${i}.fastq \
		--fastq_maxee 2 \
		--fastq_minlen 200 \
		--fastq_maxlen 500 \
		--fastqout ${OUTPUT_DIR}/filtered/${NAME}_${i}.fastq \
		--fastaout ${OUTPUT_DIR}/filtered/${NAME}_${i}.fasta
vsearch --fastq_stats ${OUTPUT_DIR}/filtered/${NAME}_${i}.fastq \
		--log ${OUTPUT_DIR}/filtered/${NAME}_${i}.stats
./2a1_preprocessing_assessment.r ${OUTPUT_DIR}/filtered/${NAME}_${i}.stats
done
#peek:
cat ${OUTPUT_DIR}/filtered/*.csv

#5.) dereplication
for i in ITS trnLF
do
vsearch --derep_fulllength ${OUTPUT_DIR}/filtered/${NAME}_${i}.fastq \
		--sizeout \
		--relabel $i \
		--output ${OUTPUT_DIR}/derepli/${NAME}_${i}.fasta \
		--log ${OUTPUT_DIR}/derepli/${NAME}_${i}.log
done


##################
### clustering ###
##################
for i in ITS trnLF
do
# minimum abundance of 0.005% (Bokulich2012: https://www-nature-com.ezp.lib.unimelb.edu.au/articles/nmeth.2276)
# minsize=$(echo $(grep "unique sequences" ${OUTPUT_DIR}/derepli/${NAME}_${i}.log | cut -d" " -f1) / 20000 + 1 | bc) # at 0.005%
minsize=$(echo $(grep "unique sequences" ${OUTPUT_DIR}/derepli/${NAME}_${i}.log | cut -d" " -f1) / 2000 + 1 | bc) # at  0.05% -->  minsize~10
# generate amplicon sequence vaiants (ASVs) instead of operational taxonomic units (OTUs)
vsearch --cluster_unoise ${OUTPUT_DIR}/derepli/${NAME}_${i}.fasta \
		--consout ${OUTPUT_DIR}/cluster/${NAME}_${i}.fasta \
		--minsize $minsize
# generate count table
vsearch --usearch_global ${OUTPUT_DIR}/filtered/${NAME}_${i}.fasta \
		--db ${OUTPUT_DIR}/cluster/${NAME}_${i}.fasta \
		--id 0.90 \
		--otutabout ${OUTPUT_DIR}/cluster/${NAME}_${i}_COUNTS.txt
done

#######################
### assign taxonomy ###
#######################
for DB_FNAME in VIRIDIPLANTAE NCBI_NT0060
do
	for i in ITS trnLF
	do
		### using GI exclusion list when using the humungous merged NCBI databases (exclude Uncultured, Environmental sample, ..., etc)
		if [ $DB_FNAME == NCBI_NT0060 ]
		then
			blastn -db ${BLASTDB}/${DB_FNAME} \
			-negative_gilist ${BLASTDB}/EXCLUDE_environ_uncult.gi \
			-query ${OUTPUT_DIR}/cluster/${NAME}_${i}.fasta \
			-perc_identity 80 \
			-outfmt '6 qseqid staxids pident evalue qcovhsp bitscore stitle' \
			-out ${OUTPUT_DIR}/taxonomy/${NAME}_${i}.${DB_FNAME}.blastemp
		else			
			blastn -db ${BLASTDB}/${DB_FNAME} \
			-query ${OUTPUT_DIR}/cluster/${NAME}_${i}.fasta \
			-perc_identity 80 \
			-outfmt '6 qseqid staxids pident evalue qcovhsp bitscore stitle' \
			-out ${OUTPUT_DIR}/taxonomy/${NAME}_${i}.${DB_FNAME}.blastemp
		fi
	done
done
# adjusted the percent identity to 80% to allow for low %identity bu better overall bit score i.e. better coverage of the query sequence
# removed -max_target_seqs in blastn so we can rank by bitscore not just percent identity

#for testing
# -num_threads 10

#find the taxon that maximizes the match per consensus sequence
for i in ITS trnLF
do
./2a3_consolidate_taxon.r ${OUTPUT_DIR}/taxonomy/ ${NAME}_${i}
done

#######
### EXECUTION:
# cd /data/Misc/Airbiota_Ed/src
# parallel --link ./05_vsearch_clulster_and_search.sh {1} {2} /data/Misc/Airbiota_Ed/vsearch_out/ ::: $(ls /data/Misc/Airbiota_Ed/seq_in/lochey2017/*R1*.fastq.gz) ::: $(ls /data/Misc/Airbiota_Ed/seq_in/lochey2017/*R2*fastq.gz)
