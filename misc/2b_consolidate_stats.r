#!/usr/bin/env Rscript

#######################################################################################
### CONSOLIDATE SEQUENCE QUALITIES AFTER PRE-PROCESSING  ACROSS ALL SAMPLES (WEEKS) ###
#######################################################################################

args = commandArgs(trailing = TRUE)
STATS_DIR=args[1]

setwd(STATS_DIR)

raw = read.csv("STATS_RAW_READS.csv", header=FALSE)
trimmed = read.csv("STATS_TRIMMED_READS.csv", header=FALSE)
merged = read.csv("STATS_MERGED_READS.csv", header=FALSE)
filtered = read.csv("STATS_FILTERED_READS.csv", header=FALSE)

for (i in c("raw", "trimmed", "merged", "filtered")){
	eval(parse(text=paste0("colnames(", i, ")=c('READ', 'NUMBER_OF_SEQUENCES', 'MEAN_SEQUENCE_LENGTH', 'MEAN_ERROR_RATE')")))
}

#number of sequences
jpeg("Histograms_number_of_sequences.jpg", quality=100, width=800, height=800)
par(mfrow=c(2,2))
hist(raw$NUMBER_OF_SEQUENCES, main="", xlab="Number of raw sequences")
hist(trimmed$NUMBER_OF_SEQUENCES, main="", xlab="Number of trimmed sequences")
hist(merged$NUMBER_OF_SEQUENCES, main="", xlab="Number of merged sequences")
hist(filtered$NUMBER_OF_SEQUENCES, main="", xlab="Number of filtered sequences")
dev.off()

#sequence lengths
jpeg("Histograms_sequences_length.jpg", quality=100, width=800, height=800)
par(mfrow=c(2,2))
hist(raw$MEAN_SEQUENCE_LENGTH, main="", xlab="Raw sequence length (bp)")
hist(trimmed$MEAN_SEQUENCE_LENGTH, main="", xlab="Trimmed sequence length (bp)")
hist(merged$MEAN_SEQUENCE_LENGTH, main="", xlab="Merged sequence length (bp)")
hist(filtered$MEAN_SEQUENCE_LENGTH, main="", xlab="Filtered sequence length (bp)")
dev.off()

#sequence quality
jpeg("Histograms_error_rate.jpg", quality=100, width=800, height=800)
par(mfrow=c(2,2))
hist(raw$MEAN_ERROR_RATE, main="", xlab="Raw sequence error rate")
hist(trimmed$MEAN_ERROR_RATE, main="", xlab="Trimmed sequence error rate")
hist(merged$MEAN_ERROR_RATE, main="", xlab="Merged sequence error rate")
hist(filtered$MEAN_ERROR_RATE, main="", xlab="Filtered sequence error rate")
dev.off()
