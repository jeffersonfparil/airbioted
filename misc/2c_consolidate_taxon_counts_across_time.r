#!/usr/bin/env Rscript

##############################
### CONSOLIDATE BY SPECIES ###
##############################
### and draw the heat maps across all species identified be it plants, fungi and others!

args = commandArgs(trailing = TRUE)
COUNTS_DIR = args[1]
TAXON_DIR = args[2]
BLASTDB = args[3]
OUTPUT_DIR = args[4]
AMPLICON = args[5]

# COUNTS_DIR = "/data/Misc/AIRBIOTED/illumina/cluster"
# TAXON_DIR = "/data/Misc/AIRBIOTED/illumina/taxonomy"
# OUTPUT_DIR = "/data/Misc/AIRBIOTED/illumina/taxonomy"
# BLASTDB = "/data/BlastDB"
# AMPLICON = "ITS"
# AMPLICON = "trnLF"
# for (AMPLICON in c("ITS", "trnLF")) {

### initialize taxonomizr
library(taxonomizr)
nodes=read.nodes(paste0(BLASTDB, "/nodes.dmp"))
names=read.names(paste0(BLASTDB, "/names.dmp"))

### consolidate taxon and their corresponding counts per week
COUNTS_LIST = system(paste0("ls ", COUNTS_DIR, "/*", AMPLICON,"_COUNTS.txt"), intern=TRUE)
TAXON_LIST = system(paste0("ls ", TAXON_DIR, "/*", AMPLICON,".blastout"), intern=TRUE)
length(COUNTS_LIST)
length(TAXON_LIST)

rm(list=c("CONSOLIDATED"))
stats.week=c(); stats.cns=c(); stats.uniden=c(); stats.abund=c(); stats.perc=c()
for (i in 1:length(COUNTS_LIST)){
	COUNTS_FILE = COUNTS_LIST[i]
	TAXON_FILE = paste0(TAXON_DIR, "/", strsplit(basename(COUNTS_FILE), "_COUNTS.txt")[[1]][1], ".blastout")
	if (file.info(COUNTS_FILE)$size==0){
		next
	} else {
		counts = read.table(COUNTS_FILE, header=FALSE, sep="\t")
		colnames(counts) = c("CONSENSUS_ID", "COUNTS")
		taxon = read.table(TAXON_FILE, header=TRUE, sep="\t")
		### assign species id given species name
		for (j in 1:nrow(taxon)){
			taxid = taxon$ORGANISM_ID[j]
			if(is.na(taxid)==TRUE){ #for the Viridiplantae ITS2 database without the taxid just gi and nicely formatted species names which we will use here
				species_name = as.character(taxon$ORGANISM[j])
				if(grepl("sp.", species_name)==TRUE | grepl(" x$", species_name)==TRUE ) { #for species names with sp. or the hybrid species e.g. Musa x paradisiaca which will not work with taxonomizr's getId fundction
					species_name = strsplit(species_name, " ")[[1]][1]
				}
				taxid = as.numeric(getId(species_name, names))
				taxon$ORGANISM_ID[j] = taxid
			}
		}
		### save the species details per week including the integral SPECIES_ID which will be used later in extracting taxon information
		MERGED = data.frame(CONSENSUS_ID=taxon$QUERY_ID, SPECIES=taxon$ORGANISM, SPECIES_ID=taxon$ORGANISM_ID)
		MERGED = merge(MERGED, counts, by="CONSENSUS_ID", all=TRUE)
		write.csv(MERGED, file=paste0(TAXON_FILE ,".FOR_CONSENSUS_SEQ_EXTRACTION.csv"), row.names=FALSE)
		### assess the number of unidentified concensus_id after blasting in both ITS2 Viridiplantae and NCBI nt00-nt60 databases
		print("====++++====++++====++++====++++====++++====++++====++++====++++====") ### How many unidentified concensus sequences are we getting?!
		week=strsplit(basename(TAXON_FILE),split=c("[.]"))[[1]][1]; stats.week = c(stats.week, week)
		cns=nrow(MERGED); stats.cns=c(stats.cns, cns)
		uniden=sum(is.na(MERGED$SPECIES), na.rm=TRUE); stats.uniden = c(stats.uniden, uniden)
		abund=sum(MERGED$COUNTS[is.na(MERGED$SPECIES)], na.rm=TRUE); stats.abund = c(stats.abund, abund)
		perc.abund=sum(MERGED$COUNTS[is.na(MERGED$SPECIES)], na.rm=TRUE)*100/sum(MERGED$COUNTS, na.rm=TRUE); stats.perc = c(stats.perc, perc.abund)
		print(week)
		print(paste0("We're getting ", uniden, " unidetified consensus sequences!"))
		print(paste0("with total abundance of ", abund," (", round(perc.abund,2), "%)."))
		print("====++++====++++====++++====++++====++++====++++====++++====++++====")
		### consolidating species name and counts into a long-form dataframe
		# consolidated = aggregate(MERGED$COUNTS ~ MERGED$SPECIES + MERGED$SPECIES_ID, FUN=sum)
		#two-step aggregation followed by merging by species name to avoid SPECIES_ID==NA exclusion which are the ones from the ITS2 database
		consolidated1 = aggregate(MERGED$COUNTS ~ MERGED$SPECIES, FUN=sum); colnames(consolidated1) = c("SPECIES", "COUNTS")
		consolidated2 = aggregate(MERGED$SPECIES_ID ~ MERGED$SPECIES, FUN=max, na.action=NULL); colnames(consolidated2) = c("SPECIES", "SPECIES_ID")
		consolidated = merge(consolidated1, consolidated2, by="SPECIES", all=FALSE)
		consolidated = data.frame(WEEK = rep(strsplit(basename(TAXON_FILE),split=c("[.]"))[[1]][1], times=nrow(consolidated)),
									SPECIES = consolidated$SPECIES,
									SPECIES_ID = consolidated$SPECIES_ID,
									COUNTS = consolidated$COUNTS)
		if(exists("CONSOLIDATED")==FALSE){
			CONSOLIDATED = consolidated
		} else {
			CONSOLIDATED = rbind(CONSOLIDATED, consolidated)
		}
	}
}

### summarize consensus sequences blatsing identification rates across weeks
STATS = data.frame(WEEK=stats.week, CONSENSUS=stats.cns, UNIDENTIFIED=stats.uniden, ABUNDANCE=stats.abund, PERC_ABUNDANCE=stats.perc)

### rename the weeks properly in the long-form consolidated data frame
weeks = levels(CONSOLIDATED$WEEK)
for (i in weeks){
	num = as.numeric(strsplit(strsplit(i, split="W")[[1]][2], split="A")[[1]][1])
	# levels(CONSOLIDATED$WEEK)[levels(CONSOLIDATED$WEEK)==i] = num
	# levels(STATS$WEEK)[levels(STATS$WEEK)==i] = num
	levels(CONSOLIDATED$WEEK) = sub(i, num, levels(CONSOLIDATED$WEEK))
	levels(STATS$WEEK) = sub(i, num, levels(STATS$WEEK))
}

### assign taxa to each species
phylum=c(); subphylum=c(); class=c(); order=c(); family=c(); genus=c(); species=c()
pb = txtProgressBar(min=0, max=nrow(CONSOLIDATED), initial=0, title="Extracting Taxonomic Classification", style=3)
for (i in 1:nrow(CONSOLIDATED)){
	taxid = CONSOLIDATED$SPECIES_ID[i]
	# if(is.na(taxid)==TRUE){ #for the Viridiplantae ITS2 database without the taxid just gi and nicely formatted species names which we will use here
	# 	species_name = as.character(CONSOLIDATED$SPECIES[i])
	# 	if(grepl("sp.", species_name)==TRUE | grepl(" x$", species_name)==TRUE ) { #for species names with sp. or the hybrid species e.g. Musa x paradisiaca which will not work with taxonomizr's getId fundction
	# 		species_name = strsplit(species_name, " ")[[1]][1]
	# 	}
	# 	taxid = as.numeric(getId(species_name, names))
	# 	CONSOLIDATED$SPECIES_ID[i] = taxid
	# }
	if(is.na(taxid)==TRUE){ #if somehow we fail to find the taxid given the species name
		taxa = data.frame(phylum=NA, subphylum=NA, class=NA, order=NA, family=NA, genus=NA, species=NA)
	} else {
		taxa = as.data.frame(getTaxonomy2(taxid, desiredTaxa = c("superkingdom", "phylum", "subphylum", "class", "order", "family","genus", "species"),nodes, names, mc.cores=10))
	}
	for (j in c("phylum", "subphylum", "class", "order", "family", "genus", "species")){
		eval(parse(text=paste0(j, " = c(", j, ", as.character(taxa$", j, "))")))
	}
	if (is.na(taxa$species)==TRUE & is.na(taxa$genus)==FALSE){ #for species names with sp. to avoid NA in the species column when the genus name is known!
		species[i] = paste0(genus[i], " sp.")
	}
	setTxtProgressBar(pb, i)
}
close(pb)

CONSOLIDATED$PHYLUM = phylum
CONSOLIDATED$SUBPHYLUM = subphylum
CONSOLIDATED$CLASS = class
CONSOLIDATED$ORDER = order
CONSOLIDATED$FAMILY = family
CONSOLIDATED$GENUS = genus
CONSOLIDATED$SPECIES = species
for (i in c("PHYLUM", "SUBPHYLUM", "CLASS", "ORDER", "FAMILY", "GENUS", "SPECIES")){
	eval(parse(text=paste0("CONSOLIDATED$", i, "= as.factor(CONSOLIDATED$", i, ")")))
}
CONSOLIDATED$WEEK = as.numeric(as.character(CONSOLIDATED$WEEK))

# # scale counts by the number of pre-processed reads
# # --> (using the "STATS_FILTERED_READS.csv" output file from "__d_consolidate_stats.r")
# if (is.na(args[6]==FALSE)){
# 	stats = read.csv(paste0(args[6], "/STATS_FILTERED_READS.csv"))
# 	# stats = read.csv("/data/Misc/AIRBIOTED/illumina/STATS_FILTERED_READS.csv")
# 	colnames(stats) = c('READ', 'NUMBER_OF_SEQUENCES', 'MEAN_SEQUENCE_LENGTH', 'MEAN_ERROR_RATE')
# 	extract_week_groups <- function(x) {as.numeric(strsplit(strsplit(strsplit(as.character(x), "_")[[1]][1], "W")[[1]][2], "A")[[1]][1])}
# 	extract_amplicon_groups <- function(x) {strsplit(as.character(x), "_")[[1]][3]}
# 	stats$week = sapply(stats$READ, FUN=extract_week_groups)
# 	stats$amplicon = sapply(stats$READ, FUN=extract_amplicon_groups)
# 	stats = subset(stats, amplicon==AMPLICON)
# 	CONSOLIDATED$COUNTS_SCALED = CONSOLIDATED$COUNTS
# 	for (i in unique(stats$week)){
# 		CONSOLIDATED$COUNTS_SCALED[CONSOLIDATED$WEEK==i] = CONSOLIDATED$COUNTS[CONSOLIDATED$WEEK==i]/stats$NUMBER_OF_SEQUENCES[stats$week==i]
# 	}
# }

# #scaled counts per week
# CONSOLIDATED$COUNTS_SCALED = CONSOLIDATED$COUNTS
# for (i in unique(CONSOLIDATED$WEEK)){
# 	CONSOLIDATED$COUNTS_SCALED[CONSOLIDATED$WEEK==i] = CONSOLIDATED$COUNTS[CONSOLIDATED$WEEK==i]/sum(CONSOLIDATED$COUNTS[CONSOLIDATED$WEEK==i])
# }

CONSOLIDATED = CONSOLIDATED[order(CONSOLIDATED$COUNTS, decreasing=TRUE), ]
CONSOLIDATED = CONSOLIDATED[order(CONSOLIDATED$WEEK, decreasing=FALSE), ]
CONSOLIDATED$log10COUNTS = log10(CONSOLIDATED$COUNTS)
write.csv(CONSOLIDATED, file=paste0(OUTPUT_DIR, "/Consolidated_taxon_counts_across_time_",AMPLICON,".csv"))

STATS$WEEK = as.numeric(as.character(STATS$WEEK))
STATS = STATS[order(STATS$WEEK, decreasing=FALSE),]
write.csv(STATS, file=paste0(OUTPUT_DIR, "/Stats_blastn_consensus_identification_",AMPLICON,".csv"), row.names=FALSE)

jpeg(paste0(OUTPUT_DIR, "/Stats_blastn_consensus_identification_",AMPLICON,".jpg"), quality=100, width=1500, height=700)
par(mfrow=c(1,2), cex=2)
plot(x=1:nrow(STATS), y=STATS$CONSENSUS, xlab="Week", ylab="Number of Consensus Sequences", main="Consensus Sequence Counts Across Time", type="b", pch=20, col="red")
plot(x=1:nrow(STATS), y=STATS$PERC_ABUNDANCE, xlab="Week", ylab="Abundance of Unidentified Amplicons (%)", main="Abundance of Unidentified Amplicons", type="b", pch=20, col="blue")
dev.off()

# heatmaps
library(ggplot2)

### per species
n = 100
nheatmaps = ceiling(nlevels(CONSOLIDATED$SPECIES) / n)
species = levels(CONSOLIDATED$SPECIES)
species = species[order(species)]
for (i in 1:nheatmaps){
	if(i==nheatmaps) {
		subset_species = species[(((i-1)*n)+1):length(species)]
	} else {
		subset_species = species[(((i-1)*n)+1):(i*n)]
	}
	SUBSET = CONSOLIDATED[CONSOLIDATED$SPECIES %in% subset_species, ]
	SUBSET = droplevels(SUBSET)
	p = ggplot(data=SUBSET, aes(x=WEEK, y=SPECIES, fill=log10COUNTS)) + 
	geom_tile() +
	scale_fill_gradient2(low="lightgreen", high="indianred1", mid="lightgoldenrodyellow", 
		# midpoint=mean(SUBSET$COUNTS,na.rm=TRUE), 
		midpoint=(max(SUBSET$log10COUNTS,na.rm=TRUE) - min(SUBSET$log10COUNTS,na.rm=TRUE))/2, 
		limit = c(min(SUBSET$log10COUNTS,na.rm=TRUE),max(SUBSET$log10COUNTS,na.rm=TRUE)),
		space = "Lab", 
	 	name=paste0("log10 ", AMPLICON, " amplicon sequence\nabundance")) +
		xlab("Week") + ylab("Species") +
	theme_gray()
	ggsave(paste0(OUTPUT_DIR, "/Airbiota_", AMPLICON,"_PER_SPECIES_set", i,".jpg"), plot=p, height=30, width=40, units="cm", dpi=300)
}

### per phylum
p = ggplot(data=CONSOLIDATED, aes(x=WEEK, y=PHYLUM, fill=log10COUNTS)) + 
	geom_tile() +
	scale_fill_gradient2(low="lightgreen", high="indianred1", mid="lightgoldenrodyellow", 
		# midpoint=mean(SUBSET$COUNTS,na.rm=TRUE), 
		midpoint=(max(CONSOLIDATED$log10COUNTS,na.rm=TRUE) - min(CONSOLIDATED$log10COUNTS,na.rm=TRUE))/2, 
		limit = c(min(CONSOLIDATED$log10COUNTS,na.rm=TRUE),max(CONSOLIDATED$log10COUNTS,na.rm=TRUE)),
		space = "Lab", 
	 	name=paste0("log10 ", AMPLICON, " amplicon sequence\nabundance")) +
		xlab("Week") + ylab("Phylum") +
	theme_gray()
ggsave(paste0(OUTPUT_DIR, "/Airbiota_", AMPLICON,"_PER_PHYLUM.jpg"), plot=p, height=30, width=40, units="cm", dpi=300)

### per subphylum (or division?)
p = ggplot(data=CONSOLIDATED, aes(x=WEEK, y=SUBPHYLUM, fill=log10COUNTS)) + 
	geom_tile() +
	scale_fill_gradient2(low="lightgreen", high="indianred1", mid="lightgoldenrodyellow", 
		# midpoint=mean(SUBSET$COUNTS,na.rm=TRUE), 
		midpoint=(max(CONSOLIDATED$log10COUNTS,na.rm=TRUE) - min(CONSOLIDATED$log10COUNTS,na.rm=TRUE))/2, 
		limit = c(min(CONSOLIDATED$log10COUNTS,na.rm=TRUE),max(CONSOLIDATED$log10COUNTS,na.rm=TRUE)),
		space = "Lab", 
	 	name=paste0("log10 ", AMPLICON, " amplicon sequence\nabundance")) +
		xlab("Week") + ylab("Subphylum") +
	theme_gray()
ggsave(paste0(OUTPUT_DIR, "/Airbiota_", AMPLICON,"_PER_SUBPHYLUM.jpg"), plot=p, height=30, width=40, units="cm", dpi=300)

### per class
p = ggplot(data=CONSOLIDATED, aes(x=WEEK, y=CLASS, fill=log10COUNTS)) + 
	geom_tile() +
	scale_fill_gradient2(low="lightgreen", high="indianred1", mid="lightgoldenrodyellow", 
		# midpoint=mean(SUBSET$COUNTS,na.rm=TRUE), 
		midpoint=(max(CONSOLIDATED$log10COUNTS,na.rm=TRUE) - min(CONSOLIDATED$log10COUNTS,na.rm=TRUE))/2, 
		limit = c(min(CONSOLIDATED$log10COUNTS,na.rm=TRUE),max(CONSOLIDATED$log10COUNTS,na.rm=TRUE)),
		space = "Lab", 
	 	name=paste0("log10 ", AMPLICON, " amplicon sequence\nabundance")) +
		xlab("Week") + ylab("Class") +
	theme_gray()
ggsave(paste0(OUTPUT_DIR, "/Airbiota_", AMPLICON,"_PER_CLASS.jpg"), plot=p, height=30, width=40, units="cm", dpi=300)

### per order
p = ggplot(data=CONSOLIDATED, aes(x=WEEK, y=ORDER, fill=log10COUNTS)) + 
	geom_tile() +
	scale_fill_gradient2(low="lightgreen", high="indianred1", mid="lightgoldenrodyellow", 
		# midpoint=mean(SUBSET$COUNTS,na.rm=TRUE), 
		midpoint=(max(CONSOLIDATED$log10COUNTS,na.rm=TRUE) - min(CONSOLIDATED$log10COUNTS,na.rm=TRUE))/2, 
		limit = c(min(CONSOLIDATED$log10COUNTS,na.rm=TRUE),max(CONSOLIDATED$log10COUNTS,na.rm=TRUE)),
		space = "Lab", 
	 	name=paste0("log10 ", AMPLICON, " amplicon sequence\nabundance")) +
		xlab("Week") + ylab("Orders") +
	theme_gray()
ggsave(paste0(OUTPUT_DIR, "/Airbiota_", AMPLICON,"_PER_ORDER.jpg"), plot=p, height=30, width=40, units="cm", dpi=300)

### per family
n = 50
nheatmaps = ceiling(nlevels(CONSOLIDATED$FAMILY) / n)
family = levels(CONSOLIDATED$FAMILY)
family = family[order(family)]
for (i in 1:nheatmaps){
	if(i==nheatmaps) {
		subset_family = family[(((i-1)*n)+1):length(family)]
	} else {
		subset_family = family[(((i-1)*n)+1):(i*n)]
	}
	SUBSET = CONSOLIDATED[CONSOLIDATED$FAMILY %in% subset_family, ]
	SUBSET = droplevels(SUBSET)
	p = ggplot(data=SUBSET, aes(x=WEEK, y=FAMILY, fill=log10COUNTS)) + 
	geom_tile() +
	scale_fill_gradient2(low="lightgreen", high="indianred1", mid="lightgoldenrodyellow", 
		# midpoint=mean(SUBSET$COUNTS,na.rm=TRUE), 
		midpoint=(max(SUBSET$log10COUNTS,na.rm=TRUE) - min(SUBSET$log10COUNTS,na.rm=TRUE))/2, 
		limit = c(min(SUBSET$log10COUNTS,na.rm=TRUE),max(SUBSET$log10COUNTS,na.rm=TRUE)),
		space = "Lab", 
	 	name=paste0("log10 ", AMPLICON, " amplicon sequence\nabundance")) +
		xlab("Week") + ylab("Families") +
	theme_gray()
	ggsave(paste0(OUTPUT_DIR, "/Airbiota_", AMPLICON,"_PER_FAMILY_set", i,".jpg"), plot=p, height=30, width=40, units="cm", dpi=300)
}

### per genus
n = 65
nheatmaps = ceiling(nlevels(CONSOLIDATED$GENUS) / n)
genus = levels(CONSOLIDATED$GENUS)
genus = genus[order(genus)]
for (i in 1:nheatmaps){
	if(i==nheatmaps) {
		subset_genus = genus[(((i-1)*n)+1):length(genus)]
	} else {
		subset_genus = genus[(((i-1)*n)+1):(i*n)]
	}
	SUBSET = CONSOLIDATED[CONSOLIDATED$GENUS %in% subset_genus, ]
	SUBSET = droplevels(SUBSET)
	p = ggplot(data=SUBSET, aes(x=WEEK, y=GENUS, fill=log10COUNTS)) + 
	geom_tile() +
	scale_fill_gradient2(low="lightgreen", high="indianred1", mid="lightgoldenrodyellow", 
		# midpoint=mean(SUBSET$COUNTS,na.rm=TRUE), 
		midpoint=(max(SUBSET$log10COUNTS,na.rm=TRUE) - min(SUBSET$log10COUNTS,na.rm=TRUE))/2, 
		limit = c(min(SUBSET$log10COUNTS,na.rm=TRUE),max(SUBSET$log10COUNTS,na.rm=TRUE)),
		space = "Lab", 
	 	name=paste0("log10 ", AMPLICON, " amplicon sequence\nabundance")) +
		xlab("Week") + ylab("Genus") +
	theme_gray()
	ggsave(paste0(OUTPUT_DIR, "/Airbiota_", AMPLICON,"_PER_GENUS_set", i,".jpg"), plot=p, height=30, width=40, units="cm", dpi=300)
}

# }

# #tests
# # BK = CONSOLIDATED
# # CONSOLIDATED$log10COUNTS = log(CONSOLIDATED$COUNTS_SCALED, exp(1))
# # CONSOLIDATED$log10COUNTS = log(CONSOLIDATED$COUNTS, exp(1))
# p = ggplot(data=CONSOLIDATED, aes(x=WEEK, y=PHYLUM, fill=log10COUNTS)) + 
# geom_tile() +
# scale_fill_gradient2(low="lightgreen", high="indianred1", mid="lightgoldenrodyellow", 
# 	# midpoint=mean(SUBSET$COUNTS,na.rm=TRUE), 
# 	midpoint=(max(CONSOLIDATED$log10COUNTS,na.rm=TRUE) - min(CONSOLIDATED$log10COUNTS,na.rm=TRUE))/2, 
# 	limit = c(min(CONSOLIDATED$log10COUNTS,na.rm=TRUE),max(CONSOLIDATED$log10COUNTS,na.rm=TRUE)),
# 	space = "Lab", 
# 	name=paste0("log10 ", AMPLICON, " amplicon sequence\nabundance")) +
# xlab("Week") + ylab("Species") +
# theme_gray()
