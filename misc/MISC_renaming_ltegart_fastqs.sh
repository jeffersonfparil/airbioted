#!/bin/bash

### MISC RENAMING THING-O

cd /data/Misc/AIRBIOTED/illumina/fastq

# #tests:
# name=W9A_S9_L001_R2_001.fastq.gz
# id=${name%.*.*}

# ### extract time integer
# id_temp1=${id%_*_*_*_*}
# id_temp2=${id_temp1::-1}
# time=${id_temp2:1}

# ### extract read number
# id_temp=${id%_*}
# read=${id_temp: -1}

# ### new name
# NEW_NAME=${id}--T${time}_R${read}.fastq.gz
# echo $NEW_NAME

for name in $(ls *.fastq.gz)
do
id=${name%.*.*}

### extract time integer
id_temp1=${id%_*_*_*_*}
id_temp2=${id_temp1::-1}
time=${id_temp2:1}

### extract read number
id_temp=${id%_*}
read=${id_temp: -1}

### rename
NEW_NAME=${id}--T${time}_R${read}.fastq.gz
mv $name $NEW_NAME
echo $id
done
