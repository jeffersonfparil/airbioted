library(shiny)

# setwd("/home/student.unimelb.edu.au/jparil/Downloads/SRCs/airbioted/src/shiny")
# setwd("/home/jeffersonfparil/Documents/airbioted/src/shiny")

# Define UI ----
ui <- fluidPage(
  titlePanel(strong("airbioted")),
  sidebarLayout(
    sidebarPanel(  fileInput("fastq", label="Upload FASTQs here", , multiple = TRUE)),
    mainPanel(
      p("Web portal for analysing aerobiological samples based on sequences of taxonomy-discriminating amplicons.", style = "font-family: 'times'; font-si16pt"),
      p(""),
      actionButton("RUN", "Run!"),
      textOutput("test"),
      img(src="workflow.jpg", height = 800, width = 500)
      # p("A new p() command starts a new paragraph. Supply a style attribute to change the format of the entire paragraph.", style = "font-family: 'times'; font-si16pt"),
      # strong("strong() makes bold text."),
      # em("em() creates italicized (i.e, emphasized) text."),
      # br(),
      # code("code displays your text similar to computer code"),
      # div("div creates segments of text with a similar style. This division of text is all blue because I passed the argument 'style = color:blue' to div", style = "color:blue"),
      # br(),
      # p("span does the same thing as div, but it works with",
      #   span("groups of words", style = "color:blue"),
      #   "that appear inside a paragraph.")
    )
  )
)

# Define server logic ----
server <- function(input, output) {
  x = reactive({
    f = input$fastq #the file link onject thingy containing:
    for (i in 1:length(f$name)){
      fname = f$name[i]      #- the filename
      fpath = f$datapath[i]  #- the local (user) path
      file.copy(fpath, file.path(paste0(getwd(), "/user_upload/"), fname), overwrite=TRUE) #copy into the server
    }
    wc = system(paste0("wc -l ", getwd(), "/user_upload/", fname , "| cut -d' ' -f1"), intern=TRUE)
    out = list(fname=fname, wc=wc)    
    return(out)
  })
  observeEvent(input$RUN, {
    output$test = renderText({paste0("Loaded file number of lines - ", x()$wc)})
  })
  
  # output$plot = renderPlot({hist(rnorm(1000))})
}

# Run the app ----
shinyApp(ui = ui, server = server)
