#!/bin/bash

###############################################################
###															###
### PRE-PROCESSING, CLUSTERING, AND TAXONOMY IDENTIFICATION ###
###															###
###############################################################

##############
###		   ###
### INPUTS ###
###		   ###
##############
INPUT_DIR=${1}				#directory where all the fastq files are located: filenames format: SOME_STRING--T#_R#.fastq.gz; where T# is the time point e.g. T1 and T34; R# is the read number i.e. R1 or R2
FORWARD_PRIMERS_LIST=${2}	#plain text file where all the forward primer/s is/are listed one line at a time (must have the same number of lines as the reverse primer/s file)
REVERSE_PRIMERS_LIST=${3}	#plain text file where all the reverse primer/s is/are listed one line at a time (must have the same number of lines as the forward primer/s file)
MIN_LENGTH=${4}				#minimum sequence (amplicon) length
MAX_LENGTH=${5}				#maximum sequence (amplicon) length
BLASTDB_LIST=${6}			#plain text file listing the full path including the filename of the BLAST database/s to use
GI_EXCLUSION_LIST=${7}		#list of GI to be excluded from the BLASTn output because they are too ambiguous, e.g. environmental samples
PERC_IDENTITY=${8}			#minimum seqiuence identity in percentage to be included in the BLASTn output
FILTER_KEYWORDS_LIST=${9}	#plain text file list of keywords to filter NCBI organism (subject) names to improve filtering (one keyword per line)
TAXON_DUMP_DIR=${10}		#directory containing the taxonomy dump files: nodes.dmp and names.dmp for obtaining the taxonomic information given species ID number
PHYLUM=${11}				#phylum to focus on
DIVISION_FAMILY_LIST=${12}	#plain text file listing each family and the corresponding division with the headers: "FAMILY" and "DIVISION", respectively
OUTPUT_DIR=${13}			#existing directory where the output will be written into

##############
### Tests: ###
##############
# INPUT_DIR=/data/Misc/AIRBIOTED/illumina/fastq
# FORWARD_PRIMERS_LIST=/data/Misc/AIRBIOTED/illumina/src/airbioted/res/ITS1_ITS2_forward_primers.txt
# REVERSE_PRIMERS_LIST=/data/Misc/AIRBIOTED/illumina/src/airbioted/res/ITS1_ITS2_reverse_primers.txt
# MIN_LENGTH=200
# MAX_LENGTH=600
# BLASTDB_LIST=/data/Misc/AIRBIOTED/illumina/src/airbioted/res/blastdb_list.txt
# GI_EXCLUSION_LIST=/data/Misc/AIRBIOTED/illumina/src/airbioted/res/exclude_environ_uncult.gi
# PERC_IDENTITY=80
# FILTER_KEYWORDS_LIST=/data/Misc/AIRBIOTED/illumina/src/airbioted/res/ITS1_ITS2_keywords_filter.txt
# TAXON_DUMP_DIR=/data/BlastDB
# PHYLUM=Streptophyta
# DIVISION_FAMILY_LIST=/data/Misc/AIRBIOTED/illumina/src/airbioted/res/division_family_list.csv
# OUTPUT_DIR=/data/Misc/AIRBIOTED/illumina/


###############################
###							###
### SETUP WORKING DIRECTORY ###
###							###
###############################
mkdir ${OUTPUT_DIR}/trimmed
mkdir ${OUTPUT_DIR}/merged
mkdir ${OUTPUT_DIR}/filtered
mkdir ${OUTPUT_DIR}/derepli
mkdir ${OUTPUT_DIR}/cluster
mkdir ${OUTPUT_DIR}/taxonomy


##############################################
###									 	   ###
### PRE-PROCESS, CLUSTER AND COUNT SPECIES ###
###										   ###
##############################################
### parallelize for each sample (read-pair) the:
##		(1.) trimming off of primer sequences with cutadapt
##		(2.) merging of the paired-end reads
##		(3.) filtering out of reads with greater than 2 expected errors (i.e. sum(P[error]), where P[E] ~ 10^(-PHRED scrores/10))
##		(4.) dereplicate sequences (i.e. non-uniqu sequences are moved and the counts number are noted)
##		(5.) assessment of resulting pre-processed sequences
##		(6.) clustering into amplicon sequence variants (ASVs) to generate a list of consensus sequences per cluster (group sequences 90% similar)
##		(7.) count the number of occurences of each consensus sequence in the pre-processed sequences
##		(8.) blast the consensus sequences to determine the species of origin (show all hits >= 80 similarity and no shortlisting)
##		(9.) consolidate blast output such that a single subject with the highest bit score (accounts for both the coverage and e-value) with similarity with similarity at least 80% is selected
##		(main_outputs)
#			- list of species corresponding to the consensus sequences observed
#			- count data of each consensus sequence
parallel --link ./1_parallel_run_per_sample.sh 	{1} {2} \
												$FORWARD_PRIMERS_LIST $REVERSE_PRIMERS_LIST \
												$MIN_LENGTH $MAX_LENGTH \
												$BLASTDB_LIST \
												$GI_EXCLUSION_LIST \
												$PERC_IDENTITY \
												$FILTER_KEYWORDS_LIST \
												$OUTPUT_DIR ::: $(ls ${INPUT_DIR}/*_R1.fastq.gz) ::: $(ls ${INPUT_DIR}/*_R2.fastq.gz)
#	INPUT:
# 		(1) FILENAME_R1:			{AMPLICON_NAME_PLUS_OTHER_IDENTIFIERS}--T{WEEK_NUMBER}_R1.fastq.gz
# 		(2) FILENAME_R2: 			{AMPLICON_NAME_PLUS_OTHER_IDENTIFIERS}--T{WEEK_NUMBER}_R2.fastq.gz
# 		(3) FORWARD_PRIMERS_LIST: 	{PLAIN_TEXT_FILE_LIST_OF_FORWARD_PRIMERS_5'_TO_3'_ONE_PER_LINE}.txt
# 		(4) REVERSE_PRIMERS_LIST: 	{PLAIN_TEXT_FILE_LIST_OF_REVERSE_PRIMERS_5'_TO_3'_ONE_PER_LINE}.txt
# 		(5) MIN_LENGTH:				{MINIMUM_SEQUENCE_LENGTH_IN_BASES}
# 		(6) MAX_LENGTH:				{MAXIMUM_SEQUENCE_LENGTH_IN_BASES}
# 		(7) BLASTDB_LIST:			{PLAIN_TEXT_FILE_LIST_OF_THE_FULL_PATH_NAMES_OF_BLAST_DATABASES_TO_USES_ONE_PER_LINE}.txt
# 		(8) GI_EXCLUSION_LIST: 		{LIST_OF_GI_NUMBERS_TO_EXCLUDED_FROM_BLAST_HITS_E.G._ENVIRONMENTAL_SAMPLES_AND_OTHER_AMBIGUOUS_SUBJECTS}
# 		(9) PERC_IDENTITY:			{MINIMUM_PERCENT_IDENTITY_OF_BLAST_HITS}
# 		(10) FILTER_KEYWORDS_LIST:	{PLAIN_TEXT_FILE_LIST_OF_THE_KEYWORDS_TO_LOOK_FOR_IN_NCBI_BASED_BLAST_DATABASE_HITS_FOR_HIGH_QUALITY_HITS_FILTERING}.txt
# 		(11) OUTPUT_DIR:			{EXISTING_DIRECTORY_WHERE_SUBDIRECTORIES_WILL_WRITTEN_AND_ALL_THE_OUTPUTS_WILL_BE_REPOSITED}
#
#	OUTPUT:
#		- trimmed, merged, filtered, dereplicated sequence files
#		- consensus sequences defining the clusters and corresponding species
#		- count data of consensus sequences in the filtered amplicons
#		- species count data
#		- sequence statistics: ${INPUT_DIR}/fastq/*.stats.csv & ${OUTPUT_DIR}/*/*.stats.csv

##########################
###					   ###
### RAREFACTION CURVES ###
###					   ###
##########################
parallel --link ./1_parallel_run_per_sample_for_rarefaction_curve.sh 	{1} {2} \
												$FORWARD_PRIMERS_LIST $REVERSE_PRIMERS_LIST \
												$MIN_LENGTH $MAX_LENGTH \
												$BLASTDB_LIST \
												$GI_EXCLUSION_LIST \
												$PERC_IDENTITY \
												$FILTER_KEYWORDS_LIST \
												$OUTPUT_DIR ::: $(ls ${INPUT_DIR}/*_R1.fastq.gz) ::: $(ls ${INPUT_DIR}/*_R2.fastq.gz)
./__rarefaction_curves_plotting__.r ${OUTPUT_DIR}/merged
#	INPUT:
# 		(1) FILENAME_R1:			{AMPLICON_NAME_PLUS_OTHER_IDENTIFIERS}--T{WEEK_NUMBER}_R1.fastq.gz
# 		(2) FILENAME_R2: 			{AMPLICON_NAME_PLUS_OTHER_IDENTIFIERS}--T{WEEK_NUMBER}_R2.fastq.gz
# 		(3) FORWARD_PRIMERS_LIST: 	{PLAIN_TEXT_FILE_LIST_OF_FORWARD_PRIMERS_5'_TO_3'_ONE_PER_LINE}.txt
# 		(4) REVERSE_PRIMERS_LIST: 	{PLAIN_TEXT_FILE_LIST_OF_REVERSE_PRIMERS_5'_TO_3'_ONE_PER_LINE}.txt
# 		(5) MIN_LENGTH:				{MINIMUM_SEQUENCE_LENGTH_IN_BASES}
# 		(6) MAX_LENGTH:				{MAXIMUM_SEQUENCE_LENGTH_IN_BASES}
# 		(7) BLASTDB_LIST:			{PLAIN_TEXT_FILE_LIST_OF_THE_FULL_PATH_NAMES_OF_BLAST_DATABASES_TO_USES_ONE_PER_LINE}.txt
# 		(8) GI_EXCLUSION_LIST: 		{LIST_OF_GI_NUMBERS_TO_EXCLUDED_FROM_BLAST_HITS_E.G._ENVIRONMENTAL_SAMPLES_AND_OTHER_AMBIGUOUS_SUBJECTS}
# 		(9) PERC_IDENTITY:			{MINIMUM_PERCENT_IDENTITY_OF_BLAST_HITS}
# 		(10) FILTER_KEYWORDS_LIST:	{PLAIN_TEXT_FILE_LIST_OF_THE_KEYWORDS_TO_LOOK_FOR_IN_NCBI_BASED_BLAST_DATABASE_HITS_FOR_HIGH_QUALITY_HITS_FILTERING}.txt
# 		(11) OUTPUT_DIR:			{EXISTING_DIRECTORY_WHERE_SUBDIRECTORIES_WILL_WRITTEN_AND_ALL_THE_OUTPUTS_WILL_BE_REPOSITED}
#
#	OUTPUT:
#		- rarefaction file per read-pair: ${OUTPUT_DIR}/merged/${NAME}.rarefaction

#################################
###							  ###
### PRE-PROCESSING STATISTICS ###
###							  ###
#################################
# consolidate stats of raw, trimmed, merged and filtered amplicons
cat ${INPUT_DIR}/*.stats.csv > ${OUTPUT_DIR}/STATS_RAW_READS.csv
cat ${OUTPUT_DIR}/trimmed/*.stats.csv > ${OUTPUT_DIR}/STATS_TRIMMED_READS.csv
cat ${OUTPUT_DIR}/merged/*.stats.csv > ${OUTPUT_DIR}/STATS_MERGED_READS.csv
cat ${OUTPUT_DIR}/filtered/*.stats.csv > ${OUTPUT_DIR}/STATS_FILTERED_READS.csv
./__consolidate_stats__.r ${OUTPUT_DIR}

###############################
###							###
### CONSOLIDATE TAXA COUNTS ###
###							###
###############################
### consolidate the species list and count data across all samples (read-pairs)
./2_consolidate_taxon_counts_across_time_and_plot_heat_maps.r 	${OUTPUT_DIR}/cluster \
																${OUTPUT_DIR}/taxonomy \
																${TAXON_DUMP_DIR} \
																${OUTPUT_DIR}/taxonomy
#	INPUT:
# 		(1) COUNTS_DIR = ${OUTPUT_DIR}/cluster
# 		(2) TAXON_DIR = ${OUTPUT_DIR}/taxonomy
#		(3) BLASTDB = ${BLASTDB} --> containing the NCBI taxonomy dump files i.e. nodes.dmp & names.dmp
# 		(4) OUTPUT_DIR = ${OUTPUT_DIR}/taxonomy
#
#	OUTPUT:
#		- BLASTn statistics
#		- Consolidated taxon counts data
#		- Heatmaps of taxon abundance based on corresponding OTU abundance per taxon level

###################################################################
###															    ###
### ANALYZING THE DYNAMICS OF TAXA-SPECIFIC AMPLICON AMBUNDANCE ###
###															    ###
###################################################################
./3_analysis.r ${OUTPUT_DIR}/taxonomy/Consolidated_taxon_counts_across_time.csv \
				${PHYLUM} \
				${DIVISION_FAMILY_LIST} \
				${OUTPUT_DIR}/taxonomy/

#	INPUT:
# 		(1) consolidated_file = ${OUTPUT_DIR}/taxonomy/Consolidated_taxon_counts_across_time.csv
#		(2) phylum = Streptophyta
# 		(3) division_family_file = ${DIVISION_FAMILY_LIST}
# 		(4) OUTPUT_DIR = ${OUTPUT_DIR}
#
#	OUTPUT:
#		- Heatmaps of taxon abundance per division within the phylum of interest
#		- Taxon abundance correlation matrices (Pearson's product moment rho and p-values)
#		- OTU tables: taxon abundance matrix across time, i.e. "Streptophyta_for_ASV_table.csv"
#		- Blastn statistics summary

##########################################
###									   ###
### CONSOLIDATE BY CONSENSUS SEQUENCES ###
###									   ###
##########################################
./4_extract_OTUs.r  ${OUTPUT_DIR}/taxonomy \
					${OUTPUT_DIR}/cluster \
					${TAXON_DUMP_DIR} \
					${PHYLUM}_for_ASV_table.csv \
					${PHYLUM} \
					${DIVISION_FAMILY_LIST}
#	INPUT:
#		(1) TAXON_DIR = ${OUTPUT_DIR}/taxonomy
# 		(2) CONSENSUS_DIR = ${OUTPUT_DIR}/cluster
# 		(3) BLASTDB = ${TAXON_DUMP_DIR}
# 		(4) for_ASV_file = Streptophyta_for_ASV_table.csv
# 		(5) phylum_filter = Streptophyta
# 		(6) division_family_file = ${DIVISION_FAMILY_LIST}
#
#	OUTPUT:
#		- TABLE 1: SEQ COUNTS PER TAXON PER WEEK
#		- TABLE 2:  OTU COUNTS PER TAXON PER WEEK
#		- TABLE 3: OTU ID LIST PER TAXON PER WEEK
#		- FASTA FILE OF CONSENSUS SEQUENCES (OTU) with their corresponding ID


########################
###					 ###
### SAMPLE EXECUTION ###
###					 ###
########################
# ./0_main.sh /data/Misc/AIRBIOTED/illumina/fastq \
# 			/data/Misc/AIRBIOTED/illumina/src/airbioted/res/ITS1_ITS2_forward_primers.txt \
# 			/data/Misc/AIRBIOTED/illumina/src/airbioted/res/ITS1_ITS2_reverse_primers.txt \
# 			200 \
# 			600 \
# 			/data/Misc/AIRBIOTED/illumina/src/airbioted/res/blastdb_list.txt \
# 			/data/Misc/AIRBIOTED/illumina/src/airbioted/res/exclude_environ_uncult.gi \
# 			80 \
# 			/data/Misc/AIRBIOTED/illumina/src/airbioted/res/ITS1_ITS2_keywords_filter.txt \
# 			/data/BlastDB \
# 			Streptophyta \
# 			/data/Misc/AIRBIOTED/illumina/src/airbioted/res/division_family_list.csv \
# 			/data/Misc/AIRBIOTED/illumina/
