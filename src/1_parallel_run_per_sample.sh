#!/bin/bash

########################################################
###													 ###
### VSEARCH, CLUSTERING AND TAXONOMIC CLASSIFICATION ###
###													 ###
########################################################
### parallelizable bash script to:
###		- assess sequence qualities
###		- trim-off primers and group by amplicon type
###		- merging the 2 reads (paired-end reads)
###		- filtering by quality and length
###		- dereplication
###		- clustering by consensus sequences and counting the number of occurence of each concensus sequence
###		- BLAST each consensus sequence to identify taxon
### per fastq read pair or per week.

#############
###		  ###
### INPUT ###
###		  ###
#############
FILENAME_R1=${1}
FILENAME_R2=${2}
FORWARD_PRIMERS_LIST=${3}
REVERSE_PRIMERS_LIST=${4}
MIN_LENGTH=${5}
MAX_LENGTH=${6}
BLASTDB_LIST=${7}
GI_EXCLUSION_LIST=${8}
PERC_IDENTITY=${9}
FILTER_KEYWORDS_LIST=${10}
OUTPUT_DIR=${11}

##############
### Tests: ###
##############
# FILENAME_R1=/data/Misc/AIRBIOTED/illumina/fastq/W1A_S1_L001_R1_001--T1_R1.fastq.gz
# FILENAME_R2=/data/Misc/AIRBIOTED/illumina/fastq/W1A_S1_L001_R2_001--T1_R2.fastq.gz
# FORWARD_PRIMERS_LIST=/data/Misc/AIRBIOTED/illumina/src/airbioted/res/ITS1_ITS2_forward_primers.txt
# REVERSE_PRIMERS_LIST=/data/Misc/AIRBIOTED/illumina/src/airbioted/res/ITS1_ITS2_reverse_primers.txt
# MIN_LENGTH=200
# MAX_LENGTH=600
# BLASTDB_LIST=/data/Misc/AIRBIOTED/illumina/src/airbioted/res/blastdb_list.txt
# GI_EXCLUSION_LIST=/data/Misc/AIRBIOTED/illumina/src/airbioted/res/exclude_environ_uncult.gi
# PERC_IDENTITY=80
# FILTER_KEYWORDS_LIST=/data/Misc/AIRBIOTED/illumina/src/airbioted/res/ITS1_ITS2_keywords_filter.txt
# OUTPUT_DIR=/data/Misc/AIRBIOTED/illumina/

###############################
### check naming convention ###
###############################
BASE_R1=$(basename $FILENAME_R1)
BASE_R2=$(basename $FILENAME_R2)
if [ ${BASE_R1%--*} == ${BASE_R2%--*} ]
then
	echo "Sequence naming convention violated."
	echo "The prefix of the read pair file names are different."
	echo "Please make sure the names are the same and the suffix is: '--T#_R#'."
	exit
fi
INPUT_DIR=${FILENAME_R1%${BASE_R1}*}
NAME=${BASE_R1%_*.*.*}

###########################################################################################################
############################### I
###							###
### SEQUENCE PRE-PROCESSING ###
###							###
###############################

#################################
### 1.) assess sequence qualities
#################################
vsearch --fastq_stats $FILENAME_R1 \
		--log $(dirname $FILENAME_R1)/${NAME}_R1.stats
vsearch --fastq_stats $FILENAME_R2 \
		--log $(dirname $FILENAME_R2)/${NAME}_R2.stats
#try plotting some pretty graphs here....
./__preprocessing_assessment__.r $(dirname $FILENAME_R1)/${NAME}_R1.stats
./__preprocessing_assessment__.r $(dirname $FILENAME_R2)/${NAME}_R2.stats

########################
### 2.) trim-off primers
########################
### 2.a.) iterate across each primer pair
NLINES_F=$(wc -l $FORWARD_PRIMERS_LIST | cut -d' ' -f1)
NLINES_R=$(wc -l $REVERSE_PRIMERS_LIST | cut -d' ' -f1)
if [ $NLINES_F -ne $NLINES_R ]
then
	echo "The forward and reverse primers list file are not the same length!"
	exit
fi
## iterate
for i in $(seq 1 $(wc -l $FORWARD_PRIMERS_LIST | cut -d' ' -f1))
do
	F=$(head -n$i $FORWARD_PRIMERS_LIST | tail -n1)
	R=$(head -n$i $REVERSE_PRIMERS_LIST | tail -n1)
	F_C=$(./__complement_or_reverseComplement__.r C $F | cut -d' ' -f2 | sed 's/"//g')
	R_C=$(./__complement_or_reverseComplement__.r C $R | cut -d' ' -f2 | sed 's/"//g')
	F_RC=$(./__complement_or_reverseComplement__.r RC $F | cut -d' ' -f2 | sed 's/"//g')
	R_RC=$(./__complement_or_reverseComplement__.r RC $R | cut -d' ' -f2 | sed 's/"//g')
	echo $i
	echo $F
	echo $F_C
	echo $F_RC
	echo $R
	echo $R_C
	echo $R_RC
#####################################################################
	#### NOT GIVING SENSIBLE RESULTS!!! 2019-02-16 ### START
#####################################################################
	### trim and group
	# ## trim forward & reverse primers and their reverse-complements
	# # remove the untrimmed
	# cutadapt --discard-untrimmed \
	# 		-g $F \
	# 		-a $R_RC \
	# 		-G $R \
	# 		-A $F_RC \
	# 		-n 2 \
	# 		-o ${OUTPUT_DIR}/trimmed/${NAME}_R1_${i}.fastq.gz \
	# 		-p ${OUTPUT_DIR}/trimmed/${NAME}_R2_${i}.fastq.gz \
	# 		$FILENAME_R1 \
	# 		$FILENAME_R2
	# # keep the untrimmed and...
	# cutadapt --discard-trimmed \
	# 		-g $F \
	# 		-a $R_RC \
	# 		-G $R \
	# 		-A $F_RC \
	# 		-n 2 \
	# 		-o ${OUTPUT_DIR}/trimmed/${NAME}_R1_untrimmed.fastq.gz \
	# 		-p ${OUTPUT_DIR}/trimmed/${NAME}_R2_untrimmed.fastq.gz \
	# 		$FILENAME_R1 \
	# 		$FILENAME_R2
	# # ... trim forward & reverse primers and their complements
	# cutadapt --discard-untrimmed \
	# 		-g $F \
	# 		-a $R_C \
	# 		-G $R \
	# 		-A $F_C \
	# 		-n 2 \
	# 		-o ${OUTPUT_DIR}/trimmed/${NAME}_R1_temp.fastq.gz \
	# 		-p ${OUTPUT_DIR}/trimmed/${NAME}_R2_temp.fastq.gz \
	# 		${OUTPUT_DIR}/trimmed/${NAME}_R1_untrimmed.fastq.gz \
	# 		${OUTPUT_DIR}/trimmed/${NAME}_R2_untrimmed.fastq.gz
### TESTS: USING COMPLEMENTS ALONE...
	cutadapt --discard-untrimmed \
			-g $F \
			-a $R_C \
			-G $R \
			-A $F_C \
			-n 2 \
			-o ${OUTPUT_DIR}/trimmed/${NAME}_R1_${i}.fastq.gz \
			-p ${OUTPUT_DIR}/trimmed/${NAME}_R2_${i}.fastq.gz \
			$FILENAME_R1 \
			$FILENAME_R2
	# ## concatenate reads trimmed with complement and reverse-complement primers
	# cat ${OUTPUT_DIR}/trimmed/${NAME}_R1_temp.fastq.gz >> ${OUTPUT_DIR}/trimmed/${NAME}_R1_${i}.fastq.gz
	# cat ${OUTPUT_DIR}/trimmed/${NAME}_R2_temp.fastq.gz >> ${OUTPUT_DIR}/trimmed/${NAME}_R2_${i}.fastq.gz
	# rm ${OUTPUT_DIR}/trimmed/${NAME}_R1_temp.fastq.gz ${OUTPUT_DIR}/trimmed/${NAME}_R1_untrimmed.fastq.gz
	# rm ${OUTPUT_DIR}/trimmed/${NAME}_R2_temp.fastq.gz ${OUTPUT_DIR}/trimmed/${NAME}_R2_untrimmed.fastq.gz
#####################################################################
	#### NOT GIVING SENSIBLE RESULTS!!! 2019-02-16 ### END
#####################################################################
done

## concatenate across all primer pairs
cat ${OUTPUT_DIR}/trimmed/${NAME}_R1_*.fastq.gz > ${OUTPUT_DIR}/trimmed/${NAME}_R1.fastq.gz
cat ${OUTPUT_DIR}/trimmed/${NAME}_R2_*.fastq.gz > ${OUTPUT_DIR}/trimmed/${NAME}_R2.fastq.gz
rm ${OUTPUT_DIR}/trimmed/${NAME}_R1_*.fastq.gz ${OUTPUT_DIR}/trimmed/${NAME}_R2_*.fastq.gz

### 2.b.) assess qualities
vsearch --fastq_stats ${OUTPUT_DIR}/trimmed/${NAME}_R1.fastq.gz \
		--log ${OUTPUT_DIR}/trimmed/${NAME}_R1.stats
vsearch --fastq_stats ${OUTPUT_DIR}/trimmed/${NAME}_R2.fastq.gz \
		--log ${OUTPUT_DIR}/trimmed/${NAME}_R2.stats
./__preprocessing_assessment__.r ${OUTPUT_DIR}/trimmed/${NAME}_R1.stats
./__preprocessing_assessment__.r ${OUTPUT_DIR}/trimmed/${NAME}_R2.stats

## peek:
cat ${INPUT_DIR}/${NAME}_*.stats.csv
cat ${OUTPUT_DIR}/trimmed/${NAME}_*.stats.csv

### 3.) merge paired-reads (and assess qualities)
vsearch --fastq_mergepairs ${OUTPUT_DIR}/trimmed/${NAME}_R1.fastq.gz \
		--reverse ${OUTPUT_DIR}/trimmed/${NAME}_R2.fastq.gz \
		--fastqout ${OUTPUT_DIR}/merged/${NAME}.fastq
vsearch --fastq_stats ${OUTPUT_DIR}/merged/${NAME}.fastq \
		--log ${OUTPUT_DIR}/merged/${NAME}.stats
./__preprocessing_assessment__.r ${OUTPUT_DIR}/merged/${NAME}.stats

## peek:
cat ${INPUT_DIR}/${NAME}_*.stats.csv
cat ${OUTPUT_DIR}/trimmed/${NAME}_*.stats.csv
cat ${OUTPUT_DIR}/merged/${NAME}.stats.csv

#############################################
### 4.) quality and sequence length filtering
############################################# 
### Note: query lengths < 200bp seem to result in low quality hits,
###		  e.g. Pinnaceae ITS amplicon (168bp) getting identified as trnL-trnF partial sequence with coverage ~50% only!
vsearch --fastq_filter ${OUTPUT_DIR}/merged/${NAME}.fastq \
		--fastq_maxee 2 \
		--fastq_minlen $MIN_LENGTH \
		--fastq_maxlen $MAX_LENGTH \
		--fastqout ${OUTPUT_DIR}/filtered/${NAME}.fastq \
		--fastaout ${OUTPUT_DIR}/filtered/${NAME}.fasta
vsearch --fastq_stats ${OUTPUT_DIR}/filtered/${NAME}.fastq \
		--log ${OUTPUT_DIR}/filtered/${NAME}.stats
./__preprocessing_assessment__.r ${OUTPUT_DIR}/filtered/${NAME}.stats

## peek:
cat ${OUTPUT_DIR}/merged/${NAME}.stats.csv
cat ${OUTPUT_DIR}/filtered/${NAME}.stats.csv


#####################
### 5.) dereplication
#####################
vsearch --derep_fulllength ${OUTPUT_DIR}/filtered/${NAME}.fastq \
--sizeout \
--output ${OUTPUT_DIR}/derepli/${NAME}.fasta \
--log ${OUTPUT_DIR}/derepli/${NAME}.log
###########################################################################################################



###########################################################################################################
################## II
###			   ###
### clustering ###
###			   ###
##################

###########################################################################################
### generate amplicon sequence vaiants (ASVs) instead of operational taxonomic units (OTUs)
###########################################################################################
# minimum abundance of 0.005% (Bokulich2012: https://www-nature-com.ezp.lib.unimelb.edu.au/articles/nmeth.2276)
# minsize=$(echo $(grep "unique sequences" ${OUTPUT_DIR}/derepli/${NAME}_${i}.log | cut -d" " -f1) / 20000 + 1 | bc) # at 0.005%
minsize=$(echo $(grep "unique sequences" ${OUTPUT_DIR}/derepli/${NAME}.log | cut -d" " -f1) / 2000 + 1 | bc) # at  0.05% -->  minsize~10
vsearch --cluster_unoise ${OUTPUT_DIR}/derepli/${NAME}.fasta \
		--consout ${OUTPUT_DIR}/cluster/${NAME}.fasta \
		--minsize $minsize

############################
### generate the count table
############################
vsearch --usearch_global ${OUTPUT_DIR}/filtered/${NAME}.fasta \
		--db ${OUTPUT_DIR}/cluster/${NAME}.fasta \
		--id 0.90 \
		--otutabout ${OUTPUT_DIR}/cluster/${NAME}_COUNTS.txt
###########################################################################################################



###########################################################################################################
####################### III
###					###
### assign taxonomy ###
###					###
#######################

##########
### blastn
##########
for DB in $(cat $BLASTDB_LIST)
do
	DB_NAME=$(basename $DB)
	echo $DB_NAME
	blastn -db $DB \
		-negative_gilist $GI_EXCLUSION_LIST \
		-query ${OUTPUT_DIR}/cluster/${NAME}.fasta \
		-perc_identity $PERC_IDENTITY \
		-outfmt '6 qseqid staxids pident evalue qcovhsp bitscore stitle' \
		-out ${OUTPUT_DIR}/taxonomy/${NAME}_${DB_NAME}.blastemp || \
	blastn -db $DB \
		-query ${OUTPUT_DIR}/cluster/${NAME}.fasta \
		-perc_identity $PERC_IDENTITY \
		-outfmt '6 qseqid staxids pident evalue qcovhsp bitscore stitle' \
		-out ${OUTPUT_DIR}/taxonomy/${NAME}_${DB_NAME}.blastemp
done
### NOTE: "command1 || command2" says if command1 fails then execute command2 instead!


##################################################################
### find the taxon that maximizes the match per consensus sequence
##################################################################
./__consolidate_taxon__.r ${OUTPUT_DIR}/taxonomy/ ${NAME} ${FILTER_KEYWORDS_LIST}
###########################################################################################################




###########################################################################################################
######################## IV
###					 ###
### SAMPLE EXECUTION ###
###					 ###
########################
# cd /data/Misc/Airbiota_Ed/src
# ./__parallel_run_per_sample__.sh /data/Misc/AIRBIOTED/illumina/fastq/W1A_S1_L001_R1_001--T1_R1.fastq.gz \
# 								 /data/Misc/AIRBIOTED/illumina/fastq/W1A_S1_L001_R2_001--T1_R2.fastq.gz \
# 								 /data/Misc/AIRBIOTED/illumina/src/airbioted/res/ITS1_ITS2_forward_primers.txt \
# 								 /data/Misc/AIRBIOTED/illumina/src/airbioted/res/ITS1_ITS2_reverse_primers.txt \
# 								 200 \
# 								 600 \
# 								 /data/Misc/AIRBIOTED/illumina/src/airbioted/res/blastdb_list.txt \
# 								 /data/Misc/AIRBIOTED/illumina/src/airbioted/res/exclude_environ_uncult.gi \
# 								 80 \
# 								 /data/Misc/AIRBIOTED/illumina/src/airbioted/res/ITS_keywords_filter.txt \
# 								 /data/Misc/AIRBIOTED/illumina/
###########################################################################################################
