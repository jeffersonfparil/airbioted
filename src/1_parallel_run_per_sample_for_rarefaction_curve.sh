#!/bin/bash

########################################################
###													 ###
### VSEARCH, CLUSTERING AND TAXONOMIC CLASSIFICATION ### FOR RAREFACTION CURVES
###													 ###
########################################################
### parallelizable bash script to:
###		- assess sequence qualities
###		- trim-off primers and group by amplicon type
###		- merging the 2 reads (paired-end reads)
###		- filtering by quality and length
###		- dereplication
###		- clustering by consensus sequences and counting the number of occurence of each concensus sequence
###		- BLAST each consensus sequence to identify taxon
### per fastq read pair or per week.

#############
###		  ###
### INPUT ###
###		  ###
#############
FILENAME_R1=${1}
FILENAME_R2=${2}
FORWARD_PRIMERS_LIST=${3}
REVERSE_PRIMERS_LIST=${4}
MIN_LENGTH=${5}
MAX_LENGTH=${6}
BLASTDB_LIST=${7}
GI_EXCLUSION_LIST=${8}
PERC_IDENTITY=${9}
FILTER_KEYWORDS_LIST=${10}
OUTPUT_DIR=${11}

##############
### Tests: ###
##############
# FILENAME_R1=/data/Misc/AIRBIOTED/illumina/fastq/W1A_S1_L001_R1_001--T1_R1.fastq.gz
# FILENAME_R2=/data/Misc/AIRBIOTED/illumina/fastq/W1A_S1_L001_R2_001--T1_R2.fastq.gz
# FORWARD_PRIMERS_LIST=/data/Misc/AIRBIOTED/illumina/src/airbioted/res/ITS1_ITS2_forward_primers.txt
# REVERSE_PRIMERS_LIST=/data/Misc/AIRBIOTED/illumina/src/airbioted/res/ITS1_ITS2_reverse_primers.txt
# MIN_LENGTH=200
# MAX_LENGTH=600
# BLASTDB_LIST=/data/Misc/AIRBIOTED/illumina/src/airbioted/res/blastdb_list.txt
# GI_EXCLUSION_LIST=/data/Misc/AIRBIOTED/illumina/src/airbioted/res/exclude_environ_uncult.gi
# PERC_IDENTITY=80
# FILTER_KEYWORDS_LIST=/data/Misc/AIRBIOTED/illumina/src/airbioted/res/ITS1_ITS2_keywords_filter.txt
# OUTPUT_DIR=/data/Misc/AIRBIOTED/illumina/

###############################
### check naming convention ###
###############################
BASE_R1=$(basename $FILENAME_R1)
BASE_R2=$(basename $FILENAME_R2)
if [ ${BASE_R1%--*} == ${BASE_R2%--*} ]
then
	echo "Sequence naming convention violated."
	echo "The prefix of the read pair file names are different."
	echo "Please make sure the names are the same and the suffix is: '--T#_R#'."
	exit
fi
INPUT_DIR=${FILENAME_R1%${BASE_R1}*}
NAME=${BASE_R1%_*.*.*}

###########################################################################################################
############################### I
###							###
### SEQUENCE PRE-PROCESSING ###
###							###
###############################

########################
### 2.) trim-off primers
########################
### 2.a.) iterate across each primer pair
NLINES_F=$(wc -l $FORWARD_PRIMERS_LIST | cut -d' ' -f1)
NLINES_R=$(wc -l $REVERSE_PRIMERS_LIST | cut -d' ' -f1)
if [ $NLINES_F -ne $NLINES_R ]
then
	echo "The forward and reverse primers list file are not the same length!"
	exit
fi
## iterate
for i in $(seq 1 $(wc -l $FORWARD_PRIMERS_LIST | cut -d' ' -f1))
do
	F=$(head -n$i $FORWARD_PRIMERS_LIST | tail -n1)
	R=$(head -n$i $REVERSE_PRIMERS_LIST | tail -n1)
	F_C=$(./__complement_or_reverseComplement__.r C $F | cut -d' ' -f2 | sed 's/"//g')
	R_C=$(./__complement_or_reverseComplement__.r C $R | cut -d' ' -f2 | sed 's/"//g')
	F_RC=$(./__complement_or_reverseComplement__.r RC $F | cut -d' ' -f2 | sed 's/"//g')
	R_RC=$(./__complement_or_reverseComplement__.r RC $R | cut -d' ' -f2 | sed 's/"//g')
	echo $i
	echo $F
	echo $F_C
	echo $F_RC
	echo $R
	echo $R_C
	echo $R_RC
	cutadapt --discard-untrimmed \
			-g $F \
			-a $R_C \
			-G $R \
			-A $F_C \
			-n 2 \
			-o ${OUTPUT_DIR}/trimmed/${NAME}_R1_${i}.fastq.gz \
			-p ${OUTPUT_DIR}/trimmed/${NAME}_R2_${i}.fastq.gz \
			$FILENAME_R1 \
			$FILENAME_R2
done

## concatenate across all primer pairs
cat ${OUTPUT_DIR}/trimmed/${NAME}_R1_*.fastq.gz > ${OUTPUT_DIR}/trimmed/${NAME}_R1.fastq.gz
cat ${OUTPUT_DIR}/trimmed/${NAME}_R2_*.fastq.gz > ${OUTPUT_DIR}/trimmed/${NAME}_R2.fastq.gz
rm ${OUTPUT_DIR}/trimmed/${NAME}_R1_*.fastq.gz ${OUTPUT_DIR}/trimmed/${NAME}_R2_*.fastq.gz

### 3.) merge paired-reads (and assess qualities)
vsearch --fastq_mergepairs ${OUTPUT_DIR}/trimmed/${NAME}_R1.fastq.gz \
		--reverse ${OUTPUT_DIR}/trimmed/${NAME}_R2.fastq.gz \
		--fastqout ${OUTPUT_DIR}/merged/${NAME}.fastq

################################################################
### SUBSET MERGED SEQUENCES FOR RAREFACTION CURVE GENERATION ###
################################################################
touch ${OUTPUT_DIR}/merged/${NAME}.rarefaction
k=20 #number of sequence groups
n=$(grep "^@" ${OUTPUT_DIR}/merged/${NAME}.fastq | wc -l) #total number of sequences
m=$(echo "$n / $k" | bc) #group size increment
for i in $(seq 100 $m $n)
do
	head -n $(echo "$i * 4" | bc) ${OUTPUT_DIR}/merged/${NAME}.fastq > ${OUTPUT_DIR}/merged/${NAME}_SUB.fastq
	#############################################
	### 4.) quality and sequence length filtering
	############################################# 
	### Note: query lengths < 200bp seem to result in low quality hits,
	###		  e.g. Pinnaceae ITS amplicon (168bp) getting identified as trnL-trnF partial sequence with coverage ~50% only!
	vsearch --fastq_filter ${OUTPUT_DIR}/merged/${NAME}_SUB.fastq \
			--fastq_maxee 2 \
			--fastq_minlen $MIN_LENGTH \
			--fastq_maxlen $MAX_LENGTH \
			--fastqout ${OUTPUT_DIR}/filtered/${NAME}_SUB.fastq \
			--fastaout ${OUTPUT_DIR}/filtered/${NAME}_SUB.fasta

	#####################
	### 5.) dereplication
	#####################
	vsearch --derep_fulllength ${OUTPUT_DIR}/filtered/${NAME}_SUB.fastq \
			--sizeout \
			--output ${OUTPUT_DIR}/derepli/${NAME}_SUB.fasta \
			--log ${OUTPUT_DIR}/derepli/${NAME}_SUB.log
	###########################################################################################################



	###########################################################################################################
	################## II
	###			   ###
	### clustering ###
	###			   ###
	##################

	###########################################################################################
	### generate amplicon sequence vaiants (ASVs) instead of operational taxonomic units (OTUs)
	###########################################################################################
	# minimum abundance of 0.005% (Bokulich2012: https://www-nature-com.ezp.lib.unimelb.edu.au/articles/nmeth.2276)
	# minsize=$(echo $(grep "unique sequences" ${OUTPUT_DIR}/derepli/${NAME}_${i}.log | cut -d" " -f1) / 20000 + 1 | bc) # at 0.005%
	# minsize=$(echo $(grep "unique sequences" ${OUTPUT_DIR}/derepli/${NAME}_SUB.log | cut -d" " -f1) / 2000 + 1 | bc) # at  0.05% -->  minsize~10
	minsize=1
	vsearch --cluster_unoise ${OUTPUT_DIR}/derepli/${NAME}_SUB.fasta \
			--consout ${OUTPUT_DIR}/cluster/${NAME}_SUB.fasta \
			--minsize $minsize
	
	##############################
	### 					   ###
	### RAREFACTION CURVE DATA ###
	### 					   ###
	##############################
	nSEQ=$(grep "^@" ${OUTPUT_DIR}/merged/${NAME}_SUB.fastq | wc -l)
	nOTU=$(grep "^>" ${OUTPUT_DIR}/cluster/${NAME}_SUB.fasta | wc -l)
	echo -e "$nSEQ,$nOTU" >> ${OUTPUT_DIR}/merged/${NAME}.rarefaction
	
	### cleanup
	rm ${OUTPUT_DIR}/merged/${NAME}_SUB.fastq
	rm ${OUTPUT_DIR}/filtered/${NAME}_SUB.fastq
	rm ${OUTPUT_DIR}/filtered/${NAME}_SUB.fasta
	rm ${OUTPUT_DIR}/derepli/${NAME}_SUB.fasta
	rm ${OUTPUT_DIR}/derepli/${NAME}_SUB.log
	rm ${OUTPUT_DIR}/cluster/${NAME}_SUB.fasta
done
