#!/usr/bin/env Rscript

###################################################################
###															    ###
### ANALYZING THE DYNAMICS OF TAXA-SPECIFIC AMPLICON AMBUNDANCE ###
###															    ###
###################################################################

args = commandArgs(trailing=TRUE)
consolidated_file = args[1]
phylum=args[2]
division_family_file = args[3]
OUTPUT_DIR = args[4]

##############
### Tests: ###
##############
# consolidated_file = "/data/Misc/AIRBIOTED/illumina/taxonomy/Consolidated_taxon_counts_across_time.csv"
# phylum = "Streptophyta"
# division_family_file = "/data/Misc/AIRBIOTED/illumina/src/airbioted/res/division_family_list.csv"
# OUTPUT_DIR = "/data/Misc/AIRBIOTED/illumina/taxonomy/"

###################################################
### load libraries and define the main function ###
###################################################
library(ggplot2)

PROCESS_CONSOLIDATED_ABUNDANCE_COUNTS_PER_SPECIES <- function(consolidated_file, phylum, division_family_file){
	CONSOLIDATED = read.csv(consolidated_file, header=TRUE)
	DIVISION_FAMILY = read.csv(division_family_file, header=TRUE)

	### Focusing on [Chlorophyta (green algae) and] Streptophyta (plants superdivision)
	# PLANTS = rbind(subset(CONSOLIDATED, PHYLUM=="Chlorophyta"), 
	# 				subset(CONSOLIDATED, PHYLUM=="Streptophyta"))
	# PLANTS = subset(CONSOLIDATED, PHYLUM=="Streptophyta")
	PLANTS = subset(CONSOLIDATED, PHYLUM==phylum)
	PLANTS = droplevels(PLANTS)

	#################################################################################################################
	#assign division
	division=c()
	for (i in 1:nrow(PLANTS)){
	# for (i in 1:30){
		family = as.character(PLANTS$FAMILY[i])
		if(is.na(family)){ #if the family classification is missing
			division = c(division, NA)
		} else {
			#update NCBI's family names (Specific for plants only!!! 2019-02-17)
			if(family=="Fabaceae"){family="Leguminosae"}
			if(family=="Chenopodiaceae"){family="Amaranthaceae"}
			if(family=="Asteraceae"){family="Compositae"}
			if(family=="Ripogonaceae"){family="Polygonaceae"}
			if(family=="Cordiaceae"){family="Boraginaceae"}
			if(family=="Amphorogynaceae"){family="Apocynaceae"}
			if(family=="Heliotropiaceae"){family="Boraginaceae"}
			if(family=="Asphodelaceae"){family="Xanthorrhoeaceae"} #Haworthia magnifica
			
			div_extract = as.character(DIVISION_FAMILY$DIVISION[!is.na(DIVISION_FAMILY$FAMILY) & DIVISION_FAMILY$FAMILY==family])
			if(family=="Mesostigmataceae"){  #fresh water green algae: this seem to not be found in the http://www.theplantlist.org/1.1/browse/
				division = c(division, "Mesostigmatophyta")
			} else if(length(div_extract)==0){ #if the family classification is present by the division_family_list.csv database has no record of that family
				division = c(division, NA)
			} else {
				division = c(division, div_extract)
			}
		}
	}

	#merge with the PLANTS dataframe
	PLANTS$DIVISION = division
	PLANTS$DIVISION = as.factor(PLANTS$DIVISION)

	#################################################################################################################
	#looking at the frequency of species grouped by division and family
	FREQ_SPECIES_BY_DIVISION = table(PLANTS$DIVISION)
	FREQ_SPECIES_BY_FAMILY = table(PLANTS$FAMILY)

	#################################################################################################################
	#for the ASVs per family across time #will try to extract consensus sequences (ASVs/ OUTs)
	AGG_FAMILY = aggregate(PLANTS$COUNTS ~ PLANTS$WEEK + PLANTS$FAMILY + PLANTS$DIVISION, FUN=sum)
	colnames(AGG_FAMILY) = c("WEEK", "FAMILY", "DIVISION", "ABUNDANCE")

	ASV_FAMILY_DIVISION_ABUNDANCE = data.frame(WEEK=1:40)
	family_list=c(); division_list=c()
	for (i in levels(AGG_FAMILY$FAMILY)){
		SUBSET = subset(AGG_FAMILY, FAMILY==i)
		sub= SUBSET[,c(1,4)]
		family_list = c(family_list, as.character(SUBSET$FAMILY[1]))
		division_list = c(division_list, as.character(SUBSET$DIVISION[1]))
		ASV_FAMILY_DIVISION_ABUNDANCE = merge(ASV_FAMILY_DIVISION_ABUNDANCE, sub, by="WEEK", all=TRUE)
		colnames(ASV_FAMILY_DIVISION_ABUNDANCE)[ncol(ASV_FAMILY_DIVISION_ABUNDANCE)] = i
	}
	ASV_FAMILY_DIVISION_ABUNDANCE = t(ASV_FAMILY_DIVISION_ABUNDANCE)
	ASV_FAMILY_DIVISION_ABUNDANCE = ASV_FAMILY_DIVISION_ABUNDANCE[2:nrow(ASV_FAMILY_DIVISION_ABUNDANCE),]
	ASV_FAMILY_DIVISION_ABUNDANCE = data.frame(cbind(DIVISION=division_list, FAMILY=family_list, ASV_FAMILY_DIVISION_ABUNDANCE))
	colnames(ASV_FAMILY_DIVISION_ABUNDANCE) = c("DIVISION", "FAMILY", paste("WEEK", 1:40, sep="_"))

	#################################################################################################################
	#subset pollen-production plants
	FLOWERING = rbind(subset(PLANTS, DIVISION=="Angiosperms"), 
					  subset(PLANTS, DIVISION=="Gymnosperms"))
	if (nrow(FLOWERING)!=0){
		FLOWERING = droplevels(FLOWERING)
	} else {
		FLOWERING = PLANTS ### if we're not actually dealing with plants (and angiosperms and gymnosperms) ### NEET TO FIX THIS!!! 2019-02-17
	}
	### NEED TO RENAME FLOWERING AND PLANT VARIABLES BECAUSE WE MAY NOT BE DEALING WITH PLANTS AT ALL!!!
	FLOWERING_FAMILIES = aggregate(FLOWERING$COUNTS ~ FLOWERING$WEEK + FLOWERING$DIVISION + FLOWERING$FAMILY, FUN=sum)
	colnames(FLOWERING_FAMILIES) = c("WEEK", "DIVISION", "FAMILY", "ABUNDANCE")
	FLOWERING_FAMILIES$log10ABUNDANCE = log(FLOWERING_FAMILIES$ABUNDANCE, 10)

	for (i in levels(FLOWERING_FAMILIES$DIVISION)){
		SUBSET = subset(FLOWERING_FAMILIES, DIVISION==i)
		p = ggplot(data=SUBSET, aes(x=WEEK, y=FAMILY, fill=log10ABUNDANCE)) + 
			geom_tile() +
			scale_fill_gradient2(low="lightgreen", high="indianred1", mid="lightgoldenrodyellow", 
				# midpoint=mean(SUBSET$COUNTS,na.rm=TRUE), 
				midpoint=(max(SUBSET$log10ABUNDANCE,na.rm=TRUE) - min(SUBSET$log10ABUNDANCE,na.rm=TRUE))/2, 
				limit = c(min(SUBSET$log10ABUNDANCE,na.rm=TRUE),max(SUBSET$log10ABUNDANCE,na.rm=TRUE)),
				space = "Lab", 
			 	name=paste0("log10 amplicon sequence\nabundance")) +
				xlab("Week") + ylab("Family") +
				ggtitle(paste0(i, " Families\nAbundance through Time")) +
			theme_gray()
		ggsave(paste0(phylum, "_", i, "_FAMILIES.jpg"), plot=p, height=30, width=40, units="cm", dpi=300)
	}
	
	FREQ_FAMILY_BY_DIVISION = table(FLOWERING_FAMILIES$DIVISION)

	##################################################################################################################
	###correlation analysis
	FLOWERING_FAMILIES_FOR_CORR = data.frame(WEEK=1:40)
	for (i in levels(FLOWERING_FAMILIES$FAMILY)){
		sub = subset(FLOWERING_FAMILIES, FAMILY==i)[,c(1,4)] #week and abundance columns
		FLOWERING_FAMILIES_FOR_CORR = merge(FLOWERING_FAMILIES_FOR_CORR, sub, by="WEEK", all=TRUE)
		colnames(FLOWERING_FAMILIES_FOR_CORR)[ncol(FLOWERING_FAMILIES_FOR_CORR)] = i
	}

	# CORR_SPARSE = cor(FLOWERING_FAMILIES_FOR_CORR[,2:ncol(FLOWERING_FAMILIES_FOR_CORR)], use="na.or.complete")
	# #not working!! TOO BIG?!?!?! for na.or.complete operation?!?!

	CORR = matrix(NA, nrow=ncol(FLOWERING_FAMILIES_FOR_CORR)-1, ncol=ncol(FLOWERING_FAMILIES_FOR_CORR)-1)
	PVAL = matrix(NA, nrow=ncol(FLOWERING_FAMILIES_FOR_CORR)-1, ncol=ncol(FLOWERING_FAMILIES_FOR_CORR)-1)
	SIG_FAM1 = c()
	SIG_FAM2 = c()
	SIG_CORR = c()
	SIG_PVAL = c()
	for (i in 1:nrow(CORR)){
		for (j in 1:nrow(CORR)){
			x = FLOWERING_FAMILIES_FOR_CORR[i+1]
			y = FLOWERING_FAMILIES_FOR_CORR[j+1]
			df = sum(!is.na(x*y))-2
			# rho = cor(x, y, use="na.or.complete")[1,1]
			rho = cor(x, y, method="spearman", use="na.or.complete")[1,1]
			t_val = rho/sqrt((1-rho^2)/df)
			p_val = 2*pt(abs(t_val), df, lower.tail=FALSE)
			CORR[i, j] = rho
			PVAL[i, j] = p_val
			if(!is.na(p_val) & p_val<0.05){
				SIG_FAM1 = c(SIG_FAM1, colnames(FLOWERING_FAMILIES_FOR_CORR)[i+1])
				SIG_FAM2 = c(SIG_FAM2, colnames(FLOWERING_FAMILIES_FOR_CORR)[i+1])
				SIG_CORR = rho
				SIG_PVAL = p_val
			}
		}
	}
	for (i in c("CORR", "PVAL")){
		eval(parse(text=paste0("rownames(", i, ") = colnames(FLOWERING_FAMILIES_FOR_CORR)[2:ncol(FLOWERING_FAMILIES_FOR_CORR)]")))
		eval(parse(text=paste0("colnames(", i, ") = colnames(FLOWERING_FAMILIES_FOR_CORR)[2:ncol(FLOWERING_FAMILIES_FOR_CORR)]")))
		eval(parse(text=paste0(i, " = as.data.frame(", i, ")")))
	}
	# SIGNIFICANT_CORRELATIONS = data.frame(FAMILY1=SIG_FAM1, FAMILY2=SIG_FAM2, CORRELATION=SIG_CORR, P_VALUE=SIG_PVAL)

	OUT = list( PLANTS=PLANTS, 
				FOR_ASV=ASV_FAMILY_DIVISION_ABUNDANCE, 
				FAMILIES=FLOWERING_FAMILIES, 
				RHO=CORR, 
				PVAL=PVAL, 
				FREQ_SPECIES_BY_DIVISION=FREQ_SPECIES_BY_DIVISION, 
				FREQ_SPECIES_BY_FAMILY=FREQ_SPECIES_BY_FAMILY,
				FREQ_FAMILY_BY_DIVISION=FREQ_FAMILY_BY_DIVISION)
	return(OUT)
}


###################
### EXECUTE!!!! ###
###################
### set working directory to the output directory
setwd(OUTPUT_DIR)
OUT = PROCESS_CONSOLIDATED_ABUNDANCE_COUNTS_PER_SPECIES(consolidated_file=consolidated_file,
														phylum=phylum,
														division_family_file=division_family_file)

###############################################
### Save OTU table and correlation matrices ###
###############################################
### ASV tables
write.csv(OUT$FOR_ASV, file=paste0(phylum, "_for_ASV_table.csv"), row.names=FALSE)
### correlations (Pearson's product-moment rho)
write.csv(OUT$RHO, file=paste0(phylum, "_CORR_Rho.csv"), row.names=TRUE)
write.csv(OUT$PVAL, file=paste0(phylum, "_CORR_p-val.csv"), row.names=TRUE)

#########################
### BLASTn statistics ###
#########################
### plot histograms of percent identity, query coverage and bit scores across time per amplicon
blastout = system("ls *.blastout", intern=TRUE)
for (i in blastout){
	dat = read.table(i, header=TRUE)
	if (exists("MERGED_BLASTOUT")==FALSE){
		MERGED_BLASTOUT = dat
	} else {
		MERGED_BLASTOUT = rbind(MERGED_BLASTOUT, dat)
	}
}
### summarize BLASTn output statistics
jpeg("Consolidated_blastn_output_stats.jpeg", quality=100, width=1000, height=700)
par(cex=1.5)
dat = MERGED_BLASTOUT
hist(dat$PERCENT_IDENTITY, xlab="PERCENT_IDENTITY", main="Identity (%)")
hist(dat$QUERY_COVERAGE, xlab="QUERY_COVERAGE", main="Query Coverage  (%)")
hist(dat$BITSCORE, xlab="BIT_SCORE", main="Bit Score")
dev.off()

########################
### Sample execution ###
########################
# ./3_analysis.r /data/Misc/AIRBIOTED/illumina/taxonomy/Consolidated_taxon_counts_across_time.csv \
# 				 Streptophyta \
# 				 /data/Misc/AIRBIOTED/illumina/src/airbioted/res/division_family_list.csv \
# 				 /data/Misc/AIRBIOTED/illumina/taxonomy/
