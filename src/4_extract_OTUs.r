#!/usr/bin/env Rscript

##########################################
###									   ###
### CONSOLIDATE BY CONSENSUS SEQUENCES ###
###									   ###
##########################################
args = commandArgs(trailing=TRUE)
TAXON_DIR = args[1]
CONSENSUS_DIR = args[2]
BLASTDB = args[3]
for_ASV_file = args[4]
phylum_filter = args[5]
division_family_file = args[6]

##############
### Tests: ###
##############
# TAXON_DIR = "/data/Misc/AIRBIOTED/illumina/taxonomy"
# CONSENSUS_DIR = "/data/Misc/AIRBIOTED/illumina/cluster"
# BLASTDB = "/data/BlastDB"
# for_ASV_file = "Streptophyta_for_ASV_table.csv"
# phylum_filter="Streptophyta"
# division_family_file = "/data/Misc/AIRBIOTED/illumina/src/airbioted/res/division_family_list.csv"

##########################################################
### load the fasta file parsing library Biostrings and ###
### the taxonomy details extraction library taxonomizr ###
##########################################################
library(Biostrings)
library(taxonomizr)

#########################################
### perpare taxonomizr taxa databases ###
#########################################
nodes=read.nodes(paste0(BLASTDB, "/nodes.dmp"))
names=read.names(paste0(BLASTDB, "/names.dmp"))

######################################
### list taxon and consensus files ###
######################################
TAXON_LIST = system(paste0("ls ", TAXON_DIR, "/*.blastout.FOR_CONSENSUS_SEQ_EXTRACTION.csv"), intern=TRUE)
CONSENSUS_LIST = system(paste0("ls ", CONSENSUS_DIR, "/*.fasta"), intern=TRUE)
length(TAXON_LIST)
length(CONSENSUS_LIST)

#########################################################
### consolidate counts per consensus file across time ###
#########################################################
rm(OUT)
for (f in 1:length(TAXON_LIST)){
	### match the taxon file with the consensus sequence file
	TAXON_FILE = TAXON_LIST[f]
	CONSENSUS_FILE = CONSENSUS_LIST[grepl( strsplit(basename(TAXON_FILE), ".blastout.FOR_CONSENSUS_SEQ_EXTRACTION.csv")[[1]][1], CONSENSUS_LIST )]
	# CONSENSUS_FILE = CONSENSUS_LIST[f] ### FIX ME!!! SHOULD BE SOMETHING LIKE THIS SO THAT TAXON AND CONSENSUS FILES MATCH PERFECTLY:
	# # COUNTS_FILE = COUNTS_LIST[i]
	# # TAXON_FILE = paste0(TAXON_DIR, "/", strsplit(basename(COUNTS_FILE), "_COUNTS.txt")[[1]][1], ".blastout")

	TAXON = read.csv(TAXON_FILE)
	TAXON = data.frame(CONSENSUS_ID=TAXON$CONSENSUS_ID, SPECIES_ID=TAXON$SPECIES_ID)
	CONSENSUS = readDNAStringSet(CONSENSUS_FILE)
	#rename CONSENSUS sequence ID to match that of the TAXON (just removing the size and seqs suffix of the fasta sequence ID)
	rename_consensus_seqid <- function(x) {strsplit(as.character(x), ";")[[1]][1]}
	ID=c(); SEQ=c()
	for (j in 1:length(CONSENSUS)){
		seq = as.character(CONSENSUS[j])
		ID = c(ID, strsplit(names(seq), ";")[[1]][1])
		SEQ = c(SEQ, as.character(seq))
	}
	CONSENSUS = data.frame(CONSENSUS_ID=ID, CONSENSUS_SEQUENCE=SEQ)
	MERGED = merge(TAXON, CONSENSUS, by="CONSENSUS_ID")

	#taxonomizr
	phylum=c(); subphylum=c(); class=c(); order=c(); family=c(); genus=c(); species=c()
	pb = txtProgressBar(min=0, max=nrow(MERGED), initial=0, title="Extracting Taxonomic Classification", style=3)
	for (i in 1:nrow(MERGED)){
		taxa = as.data.frame(getTaxonomy2(MERGED$SPECIES_ID[i], desiredTaxa = c("superkingdom", "phylum", "subphylum", "class", "order", "family","genus", "species"),nodes, names, mc.cores=10))
		for (j in c("phylum", "subphylum", "class", "order", "family", "genus", "species")){
			eval(parse(text=paste0(j, " = c(", j, ", as.character(taxa$", j, "))")))
		}
		setTxtProgressBar(pb, i)
	}
	print(paste0("Time: ", f, "/", length(TAXON_LIST)))
	close(pb)
	WEEK = as.numeric(strsplit(strsplit(strsplit(strsplit(basename(TAXON_FILE), "[.]")[[1]][1], "_")[[1]][1], "W")[[1]][2], "A")[[1]][1])
	# TAXA = data.frame(CONSENSUS_ID=MERGED$CONSENSUS_ID, AMPLICON=rep(AMPLICON,times=nrow(MERGED)), WEEK=rep(WEEK,times=nrow(MERGED)), PHYLUM=phylum, SUBPHYLUM=subphylum, CLASS=class, ORDER=order, FAMILY=family, GENUS=genus, SPECIES=species)
	TAXA = data.frame(CONSENSUS_ID=MERGED$CONSENSUS_ID, WEEK=rep(WEEK,times=nrow(MERGED)), PHYLUM=phylum, SUBPHYLUM=subphylum, CLASS=class, ORDER=order, FAMILY=family, GENUS=genus, SPECIES=species)
	MERGED = merge(TAXA, MERGED, by="CONSENSUS_ID")

	### assign DIVISIONS
	DIVISION_FAMILY = read.csv(division_family_file, header=TRUE)
	## subset only by the phylum of interest ### NEED TO MAKE THE VARIABLE NAMES MORE GENERIC FOR WHEN WE ARE NOT DEALING WITH PLANTS
	PLANTS = subset(MERGED, PHYLUM==phylum_filter)
	PLANTS = droplevels(PLANTS)
	## FOR PlANTS ONLY!!!
	if(nrow(PLANTS)>0){ #test if the subset is empty i.e. no Sptreptophytas
		division=c()
		for (i in 1:nrow(PLANTS)){
			family = as.character(PLANTS$FAMILY[i])
			if(is.na(family)){ #if the family classification is missing
				division = c(division, NA)
			} else {
				#update NCBI's family names
				if(family=="Fabaceae"){family="Leguminosae"}
				if(family=="Chenopodiaceae"){family="Amaranthaceae"}
				if(family=="Asteraceae"){family="Compositae"}
				if(family=="Ripogonaceae"){family="Polygonaceae"}
				if(family=="Cordiaceae"){family="Boraginaceae"}
				if(family=="Amphorogynaceae"){family="Apocynaceae"}
				if(family=="Heliotropiaceae"){family="Boraginaceae"}
				if(family=="Asphodelaceae"){family="Xanthorrhoeaceae"} #Haworthia magnifica
				
				div_extract = as.character(DIVISION_FAMILY$DIVISION[!is.na(DIVISION_FAMILY$FAMILY) & DIVISION_FAMILY$FAMILY==family])
				if(family=="Mesostigmataceae"){  #fresh water green algae: this seem to not be found in the http://www.theplantlist.org/1.1/browse/
					division = c(division, "Mesostigmatophyta")
				} else if(length(div_extract)==0){ #if the family classification is present by the division_family_list.csv database has no record of that family
					division = c(division, NA)
				} else {
					division = c(division, div_extract)
				}
			}
		}
		#merge with the PLANTS dataframe
		PLANTS$DIVISION = division
		PLANTS$DIVISION = as.factor(PLANTS$DIVISION)

		#OUTPUT
		if(exists("OUT")==FALSE){
			OUT = PLANTS
		} else {
			OUT = rbind(OUT, PLANTS)
		}
	} else {
		next()
	}
}

##############################################
### TABLE 1: SEQ COUNTS PER TAXON PER WEEK ###
##############################################
# SPECIES = levels(OUT$SPECIES)
# FAMILY = c()
FAMILY = levels(OUT$FAMILY)
DIVISION = c()
ASV = c()
N_ASV = c()
# for (i in SPECIES){
for (i in FAMILY){
	# sub = subset(OUT, SPECIES==i)
	sub = subset(OUT, FAMILY==i)
	DIVISION = c(DIVISION, as.character(sub$DIVISION[1]))
	ASV = c(ASV, paste(unique(sub$CONSENSUS_SEQUENCE), collapse=";"))
	N_ASV = c(N_ASV, length(unique(sub$CONSENSUS_SEQUENCE)))
}
ASV_SEQ = data.frame(FAMILY=FAMILY,
					 NUMBER_OF_ASVs=N_ASV,
					 AMPLICON_SEQUENCE_VARIANT=ASV)
### merge with the FOR_ASV output from "03_analysis.r"
FOR_ASV = read.csv(paste(TAXON_DIR, for_ASV_file, sep="/"))
ASV_OUT = merge(FOR_ASV, ASV_SEQ, by="FAMILY")
### we don't need the sequences anymore since we have tables 2 and 3 plus the fasta file of concensus sequences below
### removing the consensus sequences and counts (last 2 columns)
ASV_OUT = ASV_OUT[,1:(ncol(ASV_OUT)-2)]
### write out
### remove the temporary *_FOR_ASV file and write the final ASV table
# system(paste0("rm ", TAXON_DIR, "/", for_ASV_file))
write.csv(ASV_OUT, file=paste0(TAXON_DIR, "/", phylum_filter,"_TAXON_ABUNDANCE_PER_WEEK.csv"), row.names=FALSE)

###############################################
### TABLE 2:  ASV COUNTS PER TAXON PER WEEK ###
############################################### ### NUMBER OF WEEKS HARDCODED TO 40 WEEKS
nweeks = length(TAXON_LIST)
# nweeks = length(CONSENSUS_LIST)
FAMILY = levels(OUT$FAMILY)
DIVISION = c()
FDNASV = matrix(0, nrow=length(FAMILY), ncol=nweeks)
for (i in 1:length(FAMILY)) {
	family = FAMILY[i] # weird R subset behavior - needed to define family as a separate variable before using in the subset function to get the correct output
	sub_family = subset(OUT, FAMILY==family); sub_family = droplevels(sub_family)
	DIVISION = c(DIVISION, as.character(sub_family$DIVISION[1]))
	N_ASV = c()
	for (j in 1:nweeks) {
		sub_week = subset(sub_family, WEEK==j); sub_week = droplevels(sub_week)
		N_ASV = c(N_ASV, length(unique(sub_week$CONSENSUS_SEQUENCE)))
	}
	FDNASV[i,] = N_ASV
}
colnames(FDNASV) = paste0("WEEK", 1:nweeks)
FDNASV_OUT = data.frame(FAMILY=FAMILY, DIVISION=DIVISION, FDNASV)
FDNASV_OUT = FDNASV_OUT[order(FDNASV_OUT$FAMILY),]
### write out
write.csv(FDNASV_OUT, file=paste0(TAXON_DIR, "/", phylum_filter,"_OTU_COUNTS_PER_TAXON_PER_WEEK.csv"), row.names=FALSE)

###############################################
### TABLE 3: ASV ID LIST PER TAXON PER WEEK ###
############################################### ### NUMBER OF WEEKS HARDCODED TO 40 WEEKS AS WELL :-|
### unique ASV
ASV_SEQUENCES = levels(OUT$CONSENSUS_SEQUENCE)
ASV_SEQID = c()
### rename the OUT$CONSENSUS_SEQUENCE level names into OTU_<AMPLICON_NAME>_<CONSECUTIVE_NUMBERS>
counter = 1
pb = txtProgressBar(min=0, max=length(ASV_SEQUENCES), initial=0, style=3)
for (i in ASV_SEQUENCES){
	asv_seqid = paste("OTU", counter, sep="_")
	ASV_SEQID = c(ASV_SEQID, asv_seqid)
	levels(OUT$CONSENSUS_SEQUENCE) = sub(i, asv_seqid, levels(OUT$CONSENSUS_SEQUENCE))
	counter = counter + 1
	setTxtProgressBar(pb, counter)
}
close(pb)
### summarize into a single data frame
nweeks = length(TAXON_LIST)
# nweeks = length(CONSENSUS_LIST)
FAMILY = levels(OUT$FAMILY)
DIVISION = c()
FDASV_ID = matrix(0, nrow=length(FAMILY), ncol=nweeks)
for (i in 1:length(FAMILY)) {
	family = FAMILY[i] # weird R subset behavior - needed to define family as a separate variable before using in the subset function to get the correct output
	sub_family = subset(OUT, FAMILY==family); sub_family = droplevels(sub_family)
	DIVISION = c(DIVISION, as.character(sub_family$DIVISION[1]))
	ASV_ID = c()
	for (j in 1:nweeks) {
		sub_week = subset(sub_family, WEEK==j); sub_week = droplevels(sub_week)
		ASV_ID = c(ASV_ID, paste(sub_week$CONSENSUS_SEQUENCE, collapse="; "))
	}
	FDASV_ID[i,] = ASV_ID
}
colnames(FDASV_ID) = paste0("WEEK", 1:nweeks)
FDASV_ID_OUT = data.frame(FAMILY=FAMILY, DIVISION=DIVISION, FDASV_ID)
FDASV_ID_OUT = FDASV_ID_OUT[order(FDASV_ID_OUT$FAMILY),]
### write out
write.csv(FDASV_ID_OUT, file=paste0(TAXON_DIR, "/", phylum_filter,"_OTU_ID_PER_TAXON_PER_WEEK.csv"), row.names=FALSE)

################################################################
### WRITE OUT FASTA FILE OF CONSENSUS SEQUENCES (ASV or OTU) ###
### 			WITH THEIR CORRESPONDING ID					 ###
################################################################
OTU = as(ASV_SEQUENCES, "XStringSet")
names(OTU) = ASV_SEQID
writeXStringSet(OTU, paste0(TAXON_DIR, "/", phylum_filter,"_OTU_SEQUENCES.fasta"), format="fasta")

########################
### Sample execution ###
########################
# ./4_extract_ASVs.r  /data/Misc/AIRBIOTED/illumina/taxonomy \
# 					/data/Misc/AIRBIOTED/illumina/cluster \
# 					/data/BlastDB \
# 					Streptophyta_for_ASV_table.csv \
# 					Streptophyta \
# 					/data/Misc/AIRBIOTED/illumina/src/airbioted/res/division_family_list.csv
