#!/usr/bin/env Rscript

#############################################
### GENERATE REVERSE COMPLEMENT SEQUENCES ###
#############################################

args = commandArgs(trailing=TRUE)
ACTION = args[1] #"C" - complement of the sequence only or "RC" - reverse complement of the sequence
SEQ = args[2]
if (ACTION=="C"){
	#sequence COMPLEMENT
	print(chartr("ATCG", "TAGC", SEQ))
} else if (ACTION=="RC"){
	#sequence REVERSE COMPLEMENT
	print(chartr("ATCG", "TAGC", paste(rev(strsplit(SEQ, "")[[1]]), collapse="")))
} else {
	print("--------------------------------")
	print("Incorrect action.")
	print("C for complement alone")
	print("RC for reverse complement")
	print("--------------------------------")
	print("Examples:")
	print("./2a2_complement_or_reverseComplement.r C ATCGATACGATACC")
	print("./2a2_complement_or_reverseComplement.r RC ATCGATACGATACC")
	print("--------------------------------")
}

