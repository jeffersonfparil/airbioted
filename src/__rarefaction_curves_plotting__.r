#!/usr/bin/env Rscript

#################################################
### PLOTTING RAREFACTION CURVES PER READ-PAIR ###
#################################################

args = commandArgs(trailing=TRUE)
DIR = args[1]
# DIR="/data/Misc/AIRBIOTED/illumina/merged"

### parse *rarefaction output data
setwd(DIR)
fname_list = system("ls *.rarefaction", intern=TRUE)

rm("OUT")
for (i in 1:length(fname_list)){
	fname = fname_list[i]
	dat = tryCatch({read.csv(fname, header=FALSE)}, error=function(fname){data.frame(V1=c(0), V2=c(0))})
	print(i)
	head(dat)
	week = rep(strsplit(strsplit(fname, "A_S")[[1]][1], "W")[[1]][2], times=nrow(dat))
	out = data.frame(WEEK=as.numeric(week), READS=dat[,1], OTUs=dat[,2])
	if (exists("OUT") == FALSE) {
		OUT = out
	} else {
		OUT = rbind(OUT, out)
	}
}
write.csv(OUT, file=paste0(DIR, "/Rarefaction_data.csv"), row.names=FALSE)

### plot
WEEKS = sort(unique(OUT$WEEK))
COLOURS = rainbow(length(WEEKS))

jpeg(paste0(DIR, "/Rarefaction_plot.jpg"), quality=100, width=1200, height=1000)
par(cex=2)
plot(x=OUT$READS, y=OUT$OTUs, type="n", xlab="Number of Paired-End Reads", ylab="Number of OTUs")
for (i in WEEKS) {
	sub = subset(OUT, WEEK==i)
	points(x=sub$READS, y=sub$OTUs, type="b", pch=20, lty=1, col=COLOURS[i])
}
# legend("topright", legend=WEEKS, fill=COLOURS, ncol=4)
legend("bottomright", legend=WEEKS, fill=COLOURS, ncol=4)
dev.off()
